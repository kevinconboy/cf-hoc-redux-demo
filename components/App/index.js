import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {StyleSheet} from 'aphrodite/no-important'

import withData from '@/withData'

if (typeof window !== `undefined` && window.__NEXT_DATA__) {
  StyleSheet.rehydrate(window.__NEXT_DATA__.ids)
}

function mapStateToProps (state) {
  return {nav: state.nav, user: state.user, colors: state.colors, dialog: state.dialog}
}

class AppComponent extends React.Component {
  getChildContext = () => ({
    gutterWidth: 20
  })

  render () {
    return (
      <div className="App">
        { this.props.children }
      </div>
    )
  }
}

AppComponent.childContextTypes = {
  gutterWidth: PropTypes.number
}

export default withData(connect(mapStateToProps, {})(AppComponent))

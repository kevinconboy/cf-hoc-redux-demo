import Menu from '~/Menu'
import MenuItem from '~/Menu/MenuItem'

import Avatar from '~/Avatar'

export default () =>
  <Menu>
    <MenuItem href="/client/campus" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/OrgAvatars/east.png" size={30} />}>Eastern Campus</MenuItem>
    <MenuItem href="/client/campus" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/OrgAvatars/south.png" size={30} />}>Souther Campus</MenuItem>
    <MenuItem href="/client/campus" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/OrgAvatars/west.png" size={30} />}>Western Campus</MenuItem>
  </Menu>

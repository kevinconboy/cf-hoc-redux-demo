import Menu from '~/Menu'
import MenuItem from '~/Menu/MenuItem'

import Avatar from '~/Avatar'

export default () =>
  <Menu>
    <MenuItem href="/client" icon={<Avatar img="static/img/CF-Admin-Proto-Map/OrgAvatars/univ-foundation.png" size={30} />}>College University Foundation</MenuItem>
    <MenuItem href="/client" icon={<Avatar img="static/img/CF-Admin-Proto-Map/OrgAvatars/tlc.png" size={30} />}>The Local Crowd</MenuItem>
    <MenuItem href="/client" icon={<Avatar img="static/img/CF-Admin-Proto-Map/OrgAvatars/smithsonian.png" size={30} />}>Smith & Sonian Foundation</MenuItem>
  </Menu>

import Menu from '~/Menu'
import MenuItem from '~/Menu/MenuItem'

import Avatar from '~/Avatar'

export default () =>
  <Menu>
    <MenuItem href="/client/campus/initiative" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/InitialsAvatars/gd.png" size={30} />}>Giving Day 2017</MenuItem>
    <MenuItem href="/client/campus/initiative" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/InitialsAvatars/cf.png" size={30} />}>Campus Funder</MenuItem>
    <MenuItem href="/client/campus/initiative" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/InitialsAvatars/ewb.png" size={30} />}>Engineers Without Borders</MenuItem>
    <MenuItem href="/client/campus/initiative" icon={<Avatar img="/static/img/CF-Admin-Proto-Map/InitialsAvatars/au.png" size={30} />}>Athletes Unleashed</MenuItem>
  </Menu>

import {css} from 'aphrodite/no-important'
import PropTypes from 'prop-types'

import Button from '~/Button'
import Avatar from '~/Avatar'
import Header from '~/Header'
import {styles} from '../styles'

const Level0 = ({icon, title}) => (
  <nav className={css(styles.AppBarIcons)}>
    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img={`/static/img/${icon}`}
      />}
      href="/dashboard"
      compact
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level1]}
      borderless
      inverted
    >
      <Header
        level={2}
        selectedClassName={styles.selectedHeader}
        compact
      >Community Funded
      </Header>
    </Button>

    <Button
      href="#"
      compact
      className={[styles.AppBarButton, styles.level1, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        compact
      >{ title }
      </Header>
    </Button>
  </nav>
)

Level0.propTypes = {
  icon: PropTypes.string,
  title: PropTypes.string
}

export default Level0

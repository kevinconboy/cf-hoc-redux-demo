import PropTypes from 'prop-types'

import {styles} from '../styles'
import {css} from 'aphrodite/no-important'
import Button from '~/Button'
import Avatar from '~/Avatar'
import ChevronRightIcon from 'react-icons/lib/md/chevron-right'
import Header from '~/Header'
import BarHeader from '../BarHeader'
import ClientsPopover from './ClientsPopover'
import {colors} from '^'

const Level1 = ({icon, onPop}) => (
  <nav className={css(styles.AppBarIcons)}>

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img={`/static/img/${icon}`}
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      labelClassName={styles.AppBarButtonLabel}
      href="/dashboard"
      className={[styles.AppBarButton, styles.level1, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        selectedClassName={styles.selectedHeader}
        compact
      >Community Funded
      </Header>
    </Button>

    <BarHeader
      level={2}
      onClick={onPop}
      hasDrop
      popover={<ClientsPopover />}
    >
      <span>Clients</span>
    </BarHeader>
  </nav>
)

Level1.propTypes = {
  icon: PropTypes.string,
  onPop: PropTypes.func
}

export default Level1

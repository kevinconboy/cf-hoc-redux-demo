import PropTypes from 'prop-types'

import {styles} from '../styles'
import {css} from 'aphrodite/no-important'
import Button from '~/Button'
import Avatar from '~/Avatar'
import ChevronRightIcon from 'react-icons/lib/md/chevron-right'
import Header from '~/Header'
import BarHeader from '../BarHeader'
import InitiativesPopover from './InitiativesPopover'
import {colors} from '^'

const Level3 = ({icon, onPop, user}) => (
  <nav className={css(styles.AppBarIcons)}>

    { user.level === 1 &&
      <Button
        icon={<Avatar
          compact
          className={styles.AppBarIcon}
          size={30}
          img={`/static/img/${icon}`}
        />}
        afterIcon={
          <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
        }
        href="/dashboard"
        className={[styles.AppBarButton, styles.level1, styles.hasArrow]}
        labelClassName={styles.AppBarButtonLabel}
        borderless
        inverted
      />
    }

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/OrgAvatars/univ-foundation.png"
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      href="/client/"
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level3, styles.hasArrow]}
      borderless
      inverted
    />

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/OrgAvatars/east.png"
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      href="/client/campus/"
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level3, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        selectedClassName={styles.selectedHeader}
        compact
      >Eastern Campus
      </Header>
    </Button>

    <BarHeader
      level={4}
      onClick={onPop}
      hasDrop
      popover={<InitiativesPopover />}
    >
      <span>Initiatives</span>
    </BarHeader>

  </nav>
)

Level3.propTypes = {
  icon: PropTypes.string,
  onPop: PropTypes.func,
  user: PropTypes.obj
}

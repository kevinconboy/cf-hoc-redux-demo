import PropTypes from 'prop-types'

import {styles} from '../styles'
import {css} from 'aphrodite/no-important'
import Button from '~/Button'
import Avatar from '~/Avatar'
import ChevronRightIcon from 'react-icons/lib/md/chevron-right'
import Header from '~/Header'
import BarHeader from '../BarHeader'
import PagesPopover from './PagesPopover'
import {colors} from '^'

const Level30 = ({icon, onPop, user}) => (
  <nav className={css(styles.AppBarIcons)}>

    { user.level === 1 &&
      <Button
        icon={<Avatar
          compact
          className={styles.AppBarIcon}
          size={30}
          img={`/static/img/${icon}`}
        />}
        afterIcon={
          <ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} />
        }
        href="/dashboard"
        compact
        className={[styles.AppBarButton, styles.level1, styles.hasArrow]}
        labelClassName={styles.AppBarButtonLabel}
        borderless
        inverted
      >
        <Header
          level={2}
          compact
        >CF
        </Header>
      </Button>
    }

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/OrgAvatars/univ-foundation.png"
      />}
      afterIcon={
        <ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} />
      }
      href="/client/"
      compact
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level3, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        compact
      >College University Foundation
      </Header>

    </Button>

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/OrgAvatars/east.png"
      />}
      afterIcon={
        <ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} />
      }
      href="/client/site/"
      compact
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level3, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        compact
      >Eastern Campus
      </Header>
    </Button>

    <BarHeader
      level={4}
      onClick={onPop}
      hasDrop
      popover={<PagesPopover />}
    >
      <span>Pages</span>
    </BarHeader>

  </nav>
)

Level30.propTypes = {
  icon: PropTypes.string,
  onPop: PropTypes.func,
  user: PropTypes.obj
}

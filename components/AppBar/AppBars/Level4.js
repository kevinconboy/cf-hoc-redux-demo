import PropTypes from 'prop-types'

import {styles} from '../styles'
import {css} from 'aphrodite/no-important'
import Avatar from '~/Avatar'
import ChevronRightIcon from 'react-icons/lib/md/chevron-right'
import Header from '~/Header'
import {colors} from '^'
import Button from '~/Button'

const Level4 = ({icon, user}) => (
  <nav className={css(styles.AppBarIcons)}>

    { user.level === 1 &&
    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img={`/static/img/${icon}`}
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      href="/dashboard"
      compact
      className={[styles.AppBarButton, styles.level1, styles.hasArrow]}
      labelClassName={styles.AppBarButtonLabel}
      borderless
      inverted
    />
    }

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/OrgAvatars/univ-foundation.png"
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      href="/client/"
      compact
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level3, styles.hasArrow]}
      borderless
      inverted
    />

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/OrgAvatars/east.png"
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      href="/client/campus/"
      compact
      labelClassName={styles.AppBarButtonLabel}
      className={[styles.AppBarButton, styles.level3, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        compact
      >Eastern Campus
      </Header>
    </Button>

    <Button
      icon={<Avatar
        compact
        className={styles.AppBarIcon}
        size={30}
        img="/static/img/CF-Admin-Proto-Map/InitialsAvatars/gd.png"
      />}
      afterIcon={
        <span className={css(styles.AppBarChevronIcon)}><ChevronRightIcon style={{float: `right`, flex: 1, color: colors.blueGrey700}} size={30} /></span>
      }
      href="/client/campus/initiative"
      compact
      className={[styles.AppBarButton, styles.level4, styles.hasArrow]}
      borderless
      inverted
    >
      <Header
        level={2}
        selectedClassName={styles.selectedHeader}
        compact
      >Giving Day 2017
      </Header>
    </Button>
  </nav>
)

Level4.propTypes = {
  icon: PropTypes.string,
  user: PropTypes.obj
}

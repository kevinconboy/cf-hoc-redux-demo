import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

import Header from '~/Header'
import Avatar from '~/Avatar'
import BarHeaderDrop from './BarHeaderDrop'
import Popover from '~/Popover'

export default class BarHeader extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isPopped: false
    }
  }

  onPop = () => {
    if (this.state.isPopped) {
      this.setState({
        isPopped: false
      })
    } else {
      this.setState({
        isPopped: true
      })
    }
  }

  render () {
    return (
      <div
        onClick={this.onPop}
        className={
          css(
            styles[`level${this.props.level}`],
            styles.BarHeader,
            this.props.hasDrop && styles.hasDrop,
            this.props.transparent && styles.isTransparent,
            this.props.className
          )}>

        { this.props.userImg &&
        <Avatar size={30} circle img={this.props.userImg} />
        }

        { this.props.img &&
        <Avatar size={30} img={this.props.img} />
        }
        <Header className={styles.BarHeaderH1} level={2} compact>{ this.props.children }</Header>
        { this.props.hasDrop
          ? <BarHeaderDrop isDropped={this.state.isPopped} />
          : null
        }
        { this.props.hasDrop && this.state.isPopped &&
          <Popover
            cardLevel={this.props.popoverCardLevel}
            compact
            level={2}
            target={this}
            hAlign={this.props.hAlignDrop}
            isPopped={this.state.isPopped}
            onClose={this.onPop}>{ this.props.popover }</Popover>
        }
      </div>
    )
  }
}

BarHeader.propTypes = {
  level: PropTypes.number,
  img: PropTypes.string,
  userImg: PropTypes.string,
  hasDrop: PropTypes.bool,
  transparent: PropTypes.bool,
  popover: PropTypes.element,
  hAlignDrop: PropTypes.string,
  popoverCardLevel: PropTypes.number,
}

BarHeader.defaultProps = {
  level: 1
}

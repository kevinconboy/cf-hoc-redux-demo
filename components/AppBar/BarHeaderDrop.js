import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import {styles} from './styles'
import ArrowDownIcon from 'react-icons/lib/md/keyboard-arrow-down'

const BarHeaderDrop = ({isDropped}) =>
  <div className={css(isDropped ? styles.isDropped : null, styles.BarHeaderDrop)}>
    <ArrowDownIcon size={20} />
  </div>

BarHeaderDrop.propTypes = {
  isDropped: PropTypes.bool
}

export default BarHeaderDrop

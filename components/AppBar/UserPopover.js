import Menu from '~/Menu'
import MenuItem from '~/Menu/MenuItem'

import Avatar from '~/Avatar'

export default () =>
  <Menu >
    <MenuItem icon={<Avatar img="" size={30} />}>My Account</MenuItem>
    <MenuItem icon={<Avatar img="" size={30} />}>My Profile</MenuItem>
    <MenuItem icon={<Avatar img="" size={30} />}>My Preferences</MenuItem>
    <MenuItem icon={<Avatar img="" size={30} />}>Sign Out</MenuItem>
  </Menu>

import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {toggleDialog, handleLogin} from '@/initRedux'

import {css} from 'aphrodite/no-important'

import {styles} from './styles'

import NotificationsIcon from 'react-icons/lib/md/notifications'
import HelpIcon from 'react-icons/lib/md/help'
import SearchIcon from 'react-icons/lib/md/search'
import Button from '~/Button'
import Dialog from '~/Dialog'

import BarHeader from './BarHeader'

import UserPopover from './UserPopover'
import Level0 from './AppBars/Level0'
import Level1 from './AppBars/Level1'
import Level2 from './AppBars/Level2'
import Level3 from './AppBars/Level3'
import Level4 from './AppBars/Level4'

function mapDispatchToProps (dispatch) {
  return bindActionCreators({toggleDialog, handleLogin}, dispatch)
}

function mapStateToProps (state) {
  const username = state.auth.username
  return {
    dialog: state.dialog,
    user: state.user,
    username: username
  }
}

export class AppBarComponent extends React.Component {
  static propTypes = {
    auth: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    dialog: PropTypes.objectOf(
      PropTypes.shape({
        popped: PropTypes.boolean
      })
    )
  }

  toggleLoginDialog () {
    console.log('===> here at /AppBar/index.js')
    toggleDialog('POP')
  }

  closeDialog = e => {
    toggleDialog('CLOSE')
  }

  render () {
    console.log('render app bar', this.props.user, this.props.auth)
    const loginText = this.props.username || 'Log In'

    return (
      <div className={css(styles.AppBar)}>

        { this.props.level === 0 && <Level0 title={this.props.title} icon={this.props.icon} /> }
        { this.props.level === 1 && <Level1 title={this.props.title} icon={this.props.icon} onPop={this.onPop} /> }
        { this.props.level === 2 && <Level2 title={this.props.title} icon={this.props.icon} onPop={this.onPop} user={this.props.user} /> }
        { this.props.level === 3 && <Level3 title={this.props.title} icon={this.props.icon} onPop={this.onPop} user={this.props.user} /> }
        { this.props.level === 4 && <Level4 title={this.props.title} icon={this.props.icon} onPop={this.onPop} user={this.props.user} /> }

        { this.props.level !== 0 &&
          <div className={css(styles.AppBarLogo)}>

            <Button borderless inverted>
              <SearchIcon className={css(styles.AppBarIconSmall)} />
            </Button>

            <Button compact borderless inverted>
              <HelpIcon className={css(styles.AppBarIconSmall, styles.disabledIcon)} />
            </Button>

            <Button compact borderless inverted unread>
              <NotificationsIcon className={css(styles.AppBarIconSmall)} />
            </Button>

            <BarHeader popoverCardLevel={5} className={styles.UserMenu} hasDrop userImg="/static/img/user.png" hAlignDrop="right" transparent popover={<UserPopover />}>
              <span className={css(styles.UserName)}>{ this.props.user.name }</span>
              <span className={css(styles.UserTitle)}>{ this.props.user.title }</span>
            </BarHeader>

          </div>
        }

        { this.props.level === 0 && !this.props.captured &&
          <Button borderless onClick={this.toggleLoginDialog}>{loginText}</Button>
        }

      </div>
    )
  }
}

AppBarComponent.propTypes = {
  icon: PropTypes.string,
  user: PropTypes.object,
  level: PropTypes.number,
  title: PropTypes.string,
  captured: PropTypes.bool
}

export default connect(mapStateToProps, mapDispatchToProps)(AppBarComponent)

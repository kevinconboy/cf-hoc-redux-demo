import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  AppBar: {
    ...t.fixed,
    ...t.flex,
    background: colors.blueGrey950,
    color: colors.white500,
    zIndex: 999,
    width: `100vw`,
    height: 50
  },
  disabledIcon: {
    opacity: 0.5
  },
  AppBarIcons: {
    ...t.flex,
    ...t.items_center,
    flex: 10
  },
  AppBarButton: {
    ...t.pr0,
    ...t.pl4,
    ...t.ml1,
    ...t.relative,
    ...t.tl,
    ...t.lh_copy,
    fontFamily: `Source Sans Pro`
  },
  AppBarChevronIcon: {
    '@media( max-width: 800px )': {
      ...t.dn
    }
  },
  AppBarButtonLabel: {
    '@media( max-width: 800px )': {
      ...t.dn
    }
  },
  AppBarButtonCompact: {
    ...t.pr0,
    ...t.ml1,
    ...t.relative,
    ...t.tl,
    maxWidth: 80
  },
  level1: {
    ...t.relative,
    ...t.pl2,
    ...t.pr0,
    ...t.f4,
    color: colors.white500_50,
    zIndex: 1000
  },
  level2: {
    ...t.relative,
    ...t.pl1,
    ...t.pr0,
    ...t.f4,
    color: colors.white500_50,
    zIndex: 999
  },
  level3: {
    ...t.relative,
    ...t.pl0,
    ...t.f4,
    ...t.pr0,
    color: colors.white500_50,
    zIndex: 999
  },
  level4: {
    ...t.relative,
    ...t.pl0,
    ...t.f4,
    ...t.pr1,
    color: colors.white500_50,
    zIndex: 999
  },
  AppBarUserButton: {
    ...t.pr0,
    ...t.pl3,
    ...t.flex,
    ...t.overflow_ellipsis,
    whiteSpace: `noWrap`
  },
  NotificationsUnread: {
    fill: colors.alert500
  },
  AppBarIcon: {
    width: 30,
    height: 30,
    '@media( max-width: 800px )': {
      width: 40,
      height: 40
    }
  },
  AppBarIconSmall: {
    width: 20,
    height: 20,
    '@media( max-width: 800px )': {
      width: 20,
      height: 20
    }
  },
  AppBarLogo: {
    ...t.flex
  },
  BarHeader: {
    ...t.flex,
    ...t.items_center,
    ...t.justify_center,
    ...t.lh_title,
    ...t.pl1,
    ...t.pr2,
    color: colors.white500_50,
    height: `100%`,
    ':after': {
      ...t.dn
    },
    '@media( max-width: 800px )': {
      ...t.dn
    }
  },
  BarHeaderH1: {
    flex: 10,
    ...t.ml0
  },
  selectedHeader: {
    color: colors.white500
  },
  hasDrop: {
    ...t.pointer
  },
  BarHeaderDrop: {
    ...t.ml3,
    color: colors.white500_50,
    transition: `all 100ms ease-in-out`,
    width: 20
  },
  isDropped: {
    ...t.relative,
    transform: `rotate( -180deg )`,
    transformOrigin: `center center`
  },
  isTransparent: {
    ...t.bg_transparent
  },
  UserMenu: {
    ...t.ml3,
    ...t.mr1,
    '@media( max-width: 800px )': {
      ...t.flex
    },
    '&svg': {
      ...t.dn
    }
  },
  UserName: {
    ...t.db,
    ...t.ml2,
    ...t.f4,
    color: colors.white500,
    '@media( max-width: 800px )': {
      ...t.dn
    }
  },
  UserTitle: {
    ...t.db,
    ...t.mt1,
    ...t.ml2,
    ...t.f5,
    color: colors.white500_50,
    '@media( max-width: 800px )': {
      ...t.dn
    }
  }

})

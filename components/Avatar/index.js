import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {avatarStyles} from './styles'
import AccountIcon from 'react-icons/lib/md/person'

const Avatar = ({img, size, circle, styles, className}) => {
  if (img) {
    return <img
      className={css(
        avatarStyles.Avatar,
        styles,
        circle ? avatarStyles.isCircle : null,
        className)}
      src={img}
      style={{
        width: size,
        height: size,
        minWidth: size,
        minHeight: size}} />
  } else {
    return <AccountIcon
      className={css(className)}
      size={size} />
  }
}

Avatar.propTypes = {
  img: PropTypes.string,
  size: PropTypes.number,
  circle: PropTypes.bool,
  styles: PropTypes.obj
}

Avatar.defaultProps = {
  size: 24
}

export default Avatar

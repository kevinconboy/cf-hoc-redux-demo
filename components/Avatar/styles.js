import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const avatarStyles = StyleSheet.create({
  Avatar: {
    ...t.br3,
    borderColor: colors.white500,
    borderStyle: `solid`,
    borderWidth: 1
  },
  isCircle: {
    ...t.br_100
  }
})

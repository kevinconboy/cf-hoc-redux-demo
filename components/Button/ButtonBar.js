import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class ButtonBar extends React.Component {
  renderChildrenWithClassNames = styles => {
    const childrenWithClassNames = []

    for (let i = 0; i < this.props.children.length; i++) {
      childrenWithClassNames.push(React.cloneElement(this.props.children[ i ], {key: `button_${i}`, className: styles.ButtonBarButton}))
    }

    return childrenWithClassNames
  }

  render () {
    const styles = StyleSheet.create({
      ButtonBar: {
        ...t.flex,
        flex: 100
      },
      ButtonBarButton: {
        ...t.mr2
      }
    })

    return (
      <div className={css(styles.ButtonBar)}>{ this.renderChildrenWithClassNames(styles) }</div>
    )
  }
}

import Header from '~/Header'
import Card from '~/Card'
import Button from '~/Button'
import ExampleMatrix from '~/ExampleMatrix'
import GlobeIcon from 'react-icons/lib/md/public'

export default () =>
  <div>
    <Card weighted>
      <Header level={1} compact divider>Button</Header>
      <ExampleMatrix>
        <Button>Button</Button>
        <Button primary>Primary Button</Button>
        <Button secondary>Secondary Button</Button>
        <Button tertiary>Tertiary Button</Button>
        <Button borderless>Borderless Button</Button>
      </ExampleMatrix>
    </Card>
    <Card weighted>
      <Header level={1} compact divider>Icon Button</Header>
      <ExampleMatrix>
        <Button icon={<GlobeIcon size={20} />}>Button</Button>
        <Button icon={<GlobeIcon size={20} />} primary>Primary Button</Button>
        <Button icon={<GlobeIcon size={20} />} secondary>Secondary Button</Button>
        <Button icon={<GlobeIcon size={20} />} tertiary>Tertiary Button</Button>
        <Button icon={<GlobeIcon size={20} />} borderless>Borderless Button</Button>

      </ExampleMatrix>
      <ExampleMatrix>
        <Button icon={<GlobeIcon size={20} />} />
        <Button icon={<GlobeIcon size={20} />} primary />
        <Button icon={<GlobeIcon size={20} />} secondary />
        <Button icon={<GlobeIcon size={20} />} tertiary />
        <Button icon={<GlobeIcon size={20} />} borderless />
      </ExampleMatrix>
    </Card>
    <Card weighted>
      <Header level={1} compact divider>Compact Button</Header>
      <ExampleMatrix>
        <Button compact>Button</Button>
        <Button primary compact>Primary Button</Button>
        <Button secondary compact>Secondary Button</Button>
        <Button tertiary compact>Tertiary Button</Button>
        <Button borderless compact>Borderless Button</Button>
      </ExampleMatrix>
      <ExampleMatrix>
        <Button compact icon={<GlobeIcon size={15} />} />
        <Button primary compact icon={<GlobeIcon size={15} />} />
        <Button secondary compact icon={<GlobeIcon size={15} />} />
        <Button tertiary compact icon={<GlobeIcon size={15} />} />
        <Button borderless compact icon={<GlobeIcon size={15} />} />
      </ExampleMatrix>
    </Card>
    <Card weighted>
      <Header level={1} compact divider>Block Button</Header>
      <ExampleMatrix column>
        <Button block>Button</Button>
        <Button primary block>Primary Button</Button>
        <Button secondary block>Secondary Button</Button>
        <Button tertiary block>Tertiary Button</Button>
        <Button borderless block>Borderless Button</Button>
      </ExampleMatrix>
      <ExampleMatrix column>
        <Button block icon={<GlobeIcon size={20} />}>Button</Button>
        <Button primary block icon={<GlobeIcon size={20} />}>Primary Button</Button>
        <Button secondary block icon={<GlobeIcon size={20} />}>Secondary Button</Button>
        <Button tertiary block icon={<GlobeIcon size={20} />}>Tertiary Button</Button>
        <Button borderless block icon={<GlobeIcon size={20} />}>Borderless Button</Button>
      </ExampleMatrix>
    </Card>
    <Card>
      <Header level={1} compact divider>Centered Block Button</Header>
      <ExampleMatrix column>
        <Button block alignCenter>Button</Button>
        <Button primary block alignCenter>Primary Button</Button>
        <Button secondary block alignCenter>Secondary Button</Button>
        <Button tertiary block alignCenter>Tertiary Button</Button>
        <Button borderless block alignCenter>Borderless Button</Button>
      </ExampleMatrix>
    </Card>
  </div>

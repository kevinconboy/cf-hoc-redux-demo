import PropTypes from 'prop-types'
import CFComponent from '~/CFComponent'
import {css, StyleSheet} from 'aphrodite/no-important'
import Link from 'next/link'
import t from 'tachyons-js'

export default class Button extends CFComponent {
  constructor (props, context) {
    super(props)
    this.styles = StyleSheet.create({
      Button: {
        ...t.pa2,
        ...t.pl3,
        ...t.pr3,
        ...t.dib,
        ...t.bg_transparent,
        ...t.b__none,
        ...t.ma0,
        ...t.ba,
        ...t.br3,
        ...t.outline_0,
        ...t.f3,
        fontFamily: `Source Sans Pro`,
        color: `inherit`,
        borderColor: context.colors.grey100,
        cursor: `pointer`,
        transition: `background-color 100ms ease-in-out`
      },
      isPrimary: {
        backgroundColor: context.colors.button500,
        borderColor: context.colors.button800,
        color: context.colors.white500
      },
      isSecondary: {
        backgroundColor: context.colors.button10,
        borderColor: context.colors.button200,
        color: context.colors.button600,
        fill: context.colors.button600
      },
      isTertiary: {
        color: context.colors.button500,
        fill: context.colors.button500,
        borderColor: context.colors.button50,
        ':hover': {
          backgroundColor: context.colors.button10
        }
      },
      isIntense: {
        ...t.fw6,
        letterSpacing: `0.011em`
      },
      isAlignedCenter: {
        ...t.tc,
        ...t.db
      },
      isCompact: {
        ...t.pa1,
        ...t.pr2,
        ...t.pl1,
        ...t.f4
      },
      isWeighted: {
        ...t.mb3
      },
      isCompactLabel: {
        ...t.pl1
      },
      isCompactIcon: {
        minWidth: 16
      },
      isBlock: {
        ...t.db,
        ...t.tl,
        width: `100%`
      },
      isBorderless: {
        ...t.br0,
        ...t.b__transparent,
        ...t.bg_transparent,
        ':hover': {
          ...t.bg_transparent
        }
      },
      isDisabled: {
        opacity: 0.5
      },
      isInverted: {
        color: `inherit`
      },
      isLarge: {
        ...t.pa3
      },
      isSelected: {
        backgroundColor: context.colors.button800
      },
      isCollapsed: {
        width: 0
      },
      hasIcon: {
        ...t.pl2,
        ...t.dib
      },
      isUnread: {
        ...t.relative,
        color: context.colors.white500,
        ':after': {
          ...t.dib,
          ...t.absolute,
          ...t.br_100,
          content: `" "`,
          backgroundColor: context.colors.alert400,
          top: 12.5,
          right: 5,
          width: 7.5,
          height: 7.5,
          borderColor: context.colors.white500,
          borderWidth: 1,
          borderStyle: `solid`
        }
      },
      ButtonIcon: {
        minWidth: 20,
        ...t.flex,
        ...t.content_center,
        ...t.items_center
      },
      iconAlignment: {
        ...t.flex,
        ...t.items_center,
        ...t.w_100
      },
      afterIcon: {
        ...t.ml2,
        ...t.mr3,
        ...t.fr,
        fill: context.colors.button50
      },
      hasAfterIcon: {
        ...t.pr1
      },
      label: {
        ...t.pointer,
        flex: 10
      },
      isIconOnly: {
        ...t.pr2
      },
      isIconOnlyCompact: {
        ...t.pr1
      },
      isInline: {
        ...t.pa0,
        ...t.pl0,
        ...t.pr0
      }
    })
  }

  onClick = e => {
    if (this.props.onClick) {
      this.props.onClick(e, this.props)
    }
  }

  renderButton = styles => (
    <button className={css(
      styles.Button,
      this.props.inverted && this.styles.isInverted,
      this.props.icon && this.styles.hasIcon,
      this.props.unread && this.styles.isUnread,
      this.props.selected && this.styles.isSelected,
      this.props.large && this.styles.isLarge,
      this.props.block && this.styles.isBlock,
      this.props.compact && this.styles.isCompact,
      this.props.primary && this.styles.isPrimary,
      this.props.secondary && this.styles.isSecondary,
      this.props.tertiary && this.styles.isTertiary,
      this.props.borderless && this.styles.isBorderless,
      this.props.weighted && this.styles.isWeighted,
      this.props.disabled && this.styles.isDisabled,
      !this.props.children && this.props.icon && this.styles.isIconOnly,
      !this.props.children && this.props.icon && this.props.compact && this.styles.isIconOnlyCompact,
      this.props.afterIcon && this.styles.hasAfterIcon,
      this.props.inline && this.styles.isInline,
      this.props.intense && this.styles.isIntense,
      this.props.className)}
      onClick={this.onClick}>
      <span className={css(
        this.props.icon && this.styles.iconAlignment,
        this.props.alignCenter && this.styles.isAlignedCenter
      )}>

        { this.props.icon && <span className={css(
          styles.ButtonIcon,
          this.props.compact && this.styles.isCompactIcon
        )}>{ this.props.icon }</span> }

        { this.props.children &&
          <label className={css(
            styles.label,
            this.props.icon && this.styles.hasIcon,
            this.props.collapsed && this.styles.isCollapsed,
            this.props.compact && this.styles.isCompactLabel,
            this.props.labelClassName)
          }>{ this.props.children }</label>
        }
        { this.props.afterIcon && <span className={css(styles.afterIcon)} style={this.props.afterIconStyle}>{ this.props.afterIcon }</span> }
      </span>
    </button>
  )

  render () {
    if (this.props.href) {
      return (
        <Link href={this.props.href} prefetch={this.props.prefetch}>
          <a>
            { this.renderButton(this.styles) }
          </a>
        </Link>
      )
    } else {
      return this.renderButton(this.styles)
    }
  }
}

Button.propTypes = {
  href: PropTypes.string,
  inverted: PropTypes.bool,
  unread: PropTypes.bool,
  selected: PropTypes.bool,
  large: PropTypes.bool,
  borderless: PropTypes.bool,
  block: PropTypes.bool,
  icon: PropTypes.element,
  afterIcon: PropTypes.element,
  prefetch: PropTypes.bool,
  compact: PropTypes.bool,
  onClick: PropTypes.func,
  collapsed: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  tertiary: PropTypes.bool,
  labelClassName: PropTypes.object,
  inline: PropTypes.bool,
  intense: PropTypes.bool,
  alignCenter: PropTypes.bool,
  afterIconStyle: PropTypes.object
}

import PropTypes from 'prop-types'

export default class CFComponent extends React.Component {
  static contextTypes = {
    colors: PropTypes.object
  }

  render () {
    return (
      <div>{ this.props.children }</div>
    )
  }
}

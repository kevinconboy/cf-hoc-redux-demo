import PropTypes from 'prop-types'
import CFComponent from '~/CFComponent'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class CardFooter extends CFComponent {
  render () {
    const styles = StyleSheet.create({
      CardFooter: {
        ...t.mt3,
        ...t.pa3,
        ...t.cursor__default,
        marginLeft: `-15px`,
        marginRight: `-15px`,
        marginBottom: `-15px`,
        borderTopWidth: 1,
        borderTopColor: this.context.colors.grey10,
        borderTopStyle: `solid`
      },
      isAlignedCenter: {
        ...t.c
      },
      isBorderless: {
        borderTop: 0,
        ...t.pt0
      },
      isCompact: {
        ...t.mb0,
        ...t.br0,
        borderBottomWidth: 0,
        ':first-of-type': {
          ...t.br3,
          ...t.br__top
        },
        ':last-of-type': {
          borderBottomWidth: 1,
          ...t.br3,
          ...t.br__bottom
        }

      }
    })

    return (
      <div className={css(styles.CardFooter, this.props.alignCenter && styles.isAlignedCenter, this.props.compact ? styles.isCompact : null, this.props.borderless ? styles.isBorderless : null)}>
        { this.props.children }
      </div>
    )
  }
}

CardFooter.propTypes = {
  compact: PropTypes.bool,
  borderless: PropTypes.bool,
  alignCenter: PropTypes.bool
}

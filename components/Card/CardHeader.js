import PropTypes from 'prop-types'
import CFComponent from '~/CFComponent'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class CardHeader extends CFComponent {
  render () {
    const styles = StyleSheet.create({
      CardHeader: {
        ...t.pa3,
        ...t.cursor__default,
        ...t.w_100,
        ...t.tc,
        ...t.flex,
        ...t.justify_center,
        ...t.items_center,
        marginTop: -15,
        marginLeft: -15,
        marginRight: -15,
        minHeight: 50,
        borderBottomWidth: 1,
        borderBottomColor: this.context.colors.grey10,
        borderBottomStyle: `solid`,
        color: this.context.colors.grey200,
        backgroundSize: `cover`,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
      },

      isBorderless: {
        borderBottom: 0
      },

      CardHeaderImg: {
        width: `calc( 100% + 30px )`,
        marginTop: -15,
        marginLeft: -15,
        marginRight: -15
      }
    })

    return (
      <div
        style={{minHeight: 200, backgroundImage: this.props.img && `url(${this.props.img})`, backgroundColor: this.props.color}}
        className={
          css(
            styles.CardHeader,
            this.props.compact && styles.isCompact,
            this.props.borderless && styles.isBorderless,
            this.props.className
           )}>

        { this.props.children }

      </div>
    )
  }
}
CardHeader.propTypes = {
  compact: PropTypes.bool,
  borderless: PropTypes.bool,
  img: PropTypes.string,
  color: PropTypes.string
}

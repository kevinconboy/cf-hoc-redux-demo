import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

const CardText = ({alignCenter}, children) => {
  const styles = StyleSheet.create({
    CardText: {
      ...t.mt4,
      ...t.mb4
    },
    isAlignedCenter: {
      ...t.tc
    }
  })

  return (<p className={css(styles.CardText, alignCenter && styles.isAlignedCenter)}>{ children }</p>)
}

CardText.propTypes = {
  alignCenter: PropTypes.bool
}

export default CardText

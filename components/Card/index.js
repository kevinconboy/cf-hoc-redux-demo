import PropTypes from 'prop-types'
import CFComponent from '~/CFComponent'
import Link from 'next/link'
import t from 'tachyons-js'
import {css, StyleSheet} from 'aphrodite/no-important'

export default class Card extends CFComponent {
  render () {
    const styles = StyleSheet.create({
      Card: {
        ...t.pa3,
        color: this.context.colors.black500,
        borderStyle: `solid`,
        borderWidth: 1,
        ...t.br3
      },
      isWeighted: {
        ...t.mb3
      },
      isBlock: {
        width: 'calc(100% - 20px)'
      },
      level1: {
        borderColor: this.context.colors.grey10,
        backgroundColor: this.context.colors.white500
      },
      level2: {
        borderColor: this.context.colors.primary50,
        boxShadow: `0 0 3px ${this.context.colors.primary800}`,
        backgroundColor: this.context.colors.white500
      },
      level3: {
        backgroundColor: this.context.colors.grey200,
        borderColor: this.context.colors.grey600
      },
      level4: {
        backgroundColor: this.context.colors.grey600,
        borderColor: this.context.colors.grey700
      },
      level5: {
        backgroundColor: this.context.colors.grey975,
        borderColor: this.context.colors.grey900
      },
      level_6: {
        backgroundColor: this.context.colors.blueGrey950,
        borderColor: this.context.colors.blueGrey800
      },
      isCompact: {
        ...t.mb0,
        ...t.br0,
      },
      isStacked: {
        borderBottomWidth: 0,
        ':first-of-type': {
          ...t.br3,
          ...t.br__top
        },
        ':last-of-type': {
          borderBottomWidth: 1,
          ...t.br3,
          ...t.br__bottom
        }
      },
      isClickable: {
        ...t.pointer
      }
    })

    if (this.props.href) {
      return (
        <Link prefetch href={this.props.href}>
          <div onClick={this.props.onClick} className={
            css(
              styles.Card,
              styles.isClickable,
              this.props.borderless ? styles.isBorderless : null,
              styles[ `level${this.props.level}` ],
              this.props.className && this.props.className,
              this.props.compact ? styles.isCompact : null,
              this.props.weighted && styles.isWeighted,
              this.props.stacked && styles.isStacked,
              this.props.block && styles.isBlock
             )}>
            { this.props.children }
          </div>
        </Link>
      )
    }

    return (
      <div onClick={this.props.onClick} className={
          css(
            styles.Card,
            this.props.borderless ? styles.isBorderless : null,
            styles[ `level${this.props.level}` ],
            this.props.compact ? styles.isCompact : null,
            this.props.weighted && styles.isWeighted,
            this.props.block && styles.isBlock,
            this.props.stacked && styles.isStacked,
            this.props.className && this.props.className
           )}>
        { this.props.children }
      </div>
    )
  }
}

Card.defaultProps = {
  level: 1
}

Card.propTypes = {
  compact: PropTypes.bool,
  borderless: PropTypes.bool,
  onClick: PropTypes.func,
  level: PropTypes.number,
  href: PropTypes.string,
  image: PropTypes.string,
  weighted: PropTypes.bool,
  block: PropTypes.bool,
  stacked: PropTypes.bool
}

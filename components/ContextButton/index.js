import PropTypes from 'prop-types'
import Router from 'next/router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import Button from '~/Button'

import {setNavLevel} from '@/initRedux'

function mapDispatchToProps (dispatch) {
  return bindActionCreators({setNavLevel}, dispatch)
}

function mapStateToProps (state) {
  return {nav: state.nav}
}

class ContextButton extends React.Component {
  onClick = () => {
    setNavLevel(this.props.level)
    Router.push(this.props.href)
  }

  render () {
    return (
      <Button {...this.props} onClick={this.onClick}>{ this.props.children }</Button>
    )
  }
}

ContextButton.propTypes = {
  level: PropTypes.number,
  href: PropTypes.string
}

export default connect(mapStateToProps, mapDispatchToProps)(ContextButton)

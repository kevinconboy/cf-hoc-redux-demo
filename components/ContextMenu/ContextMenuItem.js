import CFComponent from '~/CFComponent'
import MenuItem from '~/Menu/MenuItem'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {toggleNav} from '@/initRedux'

function mapDispatchToProps (dispatch) {
  return bindActionCreators({toggleNav}, dispatch)
}

function mapStateToProps (state) {
  return {nav: state.nav}
}

class ContextMenuItemComponent extends CFComponent {
  render () {
    const {children, ...newProps} = this.props
    const styles = StyleSheet.create({
      ContextMenuItem: {
        ...t.ml1,
        ...t.br__left,
        ...t.pl1,
        borderLeftWidth: 0,
        borderBottomColor: this.context.colors.white500_50,
        whiteSpace: `nowrap`
      },
      ContextMenuLabel: {
        transition: `all 10ms ease-out`,
        fontFamily: `Source Sans Pro`
      },
      ContextMenuLabelCollapsed: {
        opacity: 0,
        width: 0
      }
    })

    return (
      <MenuItem
        {...newProps}
        className={[styles.ContextMenuItem, this.props.className]}>
        <label className={css(styles.ContextMenuLabel, this.props.nav.collapsed && styles.ContextMenuLabelCollapsed)}>{ this.props.children }</label>
      </MenuItem>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContextMenuItemComponent)

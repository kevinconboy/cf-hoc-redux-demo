import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import ChevronLeftIcon from 'react-icons/lib/md/chevron-left'

import ContextMenuItem from '../ContextMenuItem'
import Menu from '~/Menu'

const menu = [
  {
    name: `dashboard`,
    divider: true,
    href: `/`,
    label: `Dashboard`
  },
  {
    name: `clients`,
    divider: false,
    href: `/clients`,
    label: `Clients`
  },
  {
    name: `people`,
    divider: false,
    href: `/people`,
    label: `People`
  },
  {
    name: `analytics`,
    divider: true,
    href: `/analytics`,
    label: `Analytics`
  },
  {
    name: `apps`,
    divider: false,
    href: `/apps`,
    label: `Apps`
  },
  {
    name: `settings`,
    divider: false,
    href: `/settings`,
    label: `Settings`
  }
]

const Level1 = ({selectedIndex, nav, toggleNav, styles, renderItems}) => (
  <Menu selectedIndex={selectedIndex} collapsed={nav.collapsed}>
    { renderItems(menu) }
    <ContextMenuItem onClick={toggleNav} icon={<ChevronLeftIcon className={css(styles.collapseExpandIcon, nav.collapsed ? styles.iconIsCollapsed : styles.iconIsExpanded)} size={20} />}>Collapse</ContextMenuItem>
  </Menu>
)

Level1.propTypes = {
  selectedIndex: PropTypes.number,
  nav: PropTypes.obj,
  toggleNav: PropTypes.func,
  styles: PropTypes.obj,
  renderItems: PropTypes.func
}

export default Level1

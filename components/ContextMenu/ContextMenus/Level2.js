import {css} from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import Menu from '~/Menu'

import ChevronLeftIcon from 'react-icons/lib/md/chevron-left'
import ContextMenuItem from '../ContextMenuItem'

const menu = [
  {
    name: `dashboard`,
    divider: true,
    href: `/client`,
    label: `Dashboard`
  },
  {
    name: `clients`,
    divider: false,
    href: `/client/campuses`,
    label: `Campuses`
  },
  {
    name: `people`,
    divider: false,
    href: `/client/people`,
    label: `People`
  },
  {
    name: `analytics`,
    divider: true,
    href: `/client/analytics`,
    label: `Analytics`
  },

  {
    name: `apps`,
    divider: false,
    href: `/client/apps`,
    label: `Apps`
  },
  {
    name: `settings`,
    divider: false,
    href: `/client/settings`,
    label: `Settings`
  }
]

const Level2 = ({selectedIndex, nav, toggleNav, styles, renderItems}) => (
  <Menu selectedIndex={selectedIndex} collapsed={nav.collapsed}>
    { renderItems(menu) }
    <ContextMenuItem onClick={toggleNav} icon={<ChevronLeftIcon className={css(styles.collapseExpandIcon, nav.collapsed ? styles.iconIsCollapsed : styles.iconIsExpanded)} size={20} />}>Collapse</ContextMenuItem>
  </Menu>
)

Level2.propTypes = {
  selectedIndex: PropTypes.number,
  nav: PropTypes.obj,
  toggleNav: PropTypes.func,
  styles: PropTypes.obj,
  renderItems: PropTypes.func
}

export default Level2

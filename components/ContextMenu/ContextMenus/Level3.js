import {css} from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import Menu from '~/Menu'

import ChevronLeftIcon from 'react-icons/lib/md/chevron-left'
import ContextMenuItem from '../ContextMenuItem'

const menu = [
  {
    name: `dashboard`,
    divider: true,
    href: `/client/campus`,
    label: `Dashboard`
  },

  {
    name: `stories`,
    divider: false,
    href: `/client/campus/initiatives`,
    label: `Initiatives`
  },
  {
    name: `people`,
    divider: false,
    href: `/client/campus/people`,
    label: `People`
  },
  {
    name: `stories`,
    divider: false,
    href: `/client/campus/stories`,
    label: `Stories`
  },
  {
    name: `channels`,
    divider: false,
    href: `/client/campus/channels`,
    label: `Channels`
  },
  {
    name: `analytics`,
    divider: true,
    href: `/client/campus/analytics`,
    label: `Analytics`
  },
  {
    name: `apps`,
    divider: false,
    href: `/client/campus/apps`,
    label: `Apps`
  },
  {
    name: `settings`,
    divider: false,
    href: `/client/campus/settings`,
    label: `Settings`
  }
]

const Level3 = ({selectedIndex, nav, toggleNav, styles, renderItems}) => (
  <Menu selectedIndex={selectedIndex} collapsed={nav.collapsed}>
    { renderItems(menu) }
    <ContextMenuItem onClick={toggleNav} icon={<ChevronLeftIcon className={css(styles.collapseExpandIcon, nav.collapsed ? styles.iconIsCollapsed : styles.iconIsExpanded)} size={20} />}>Collapse</ContextMenuItem>
  </Menu>
)

Level3.propTypes = {
  selectedIndex: PropTypes.number,
  nav: PropTypes.obj,
  toggleNav: PropTypes.func,
  styles: PropTypes.obj,
  renderItems: PropTypes.func
}

export default Level3

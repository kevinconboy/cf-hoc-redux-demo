import {css} from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import Menu from '~/Menu'

import ChevronLeftIcon from 'react-icons/lib/md/chevron-left'
import ContextMenuItem from '../ContextMenuItem'

const menu = [
  {
    name: `dashboard`,
    divider: true,
    href: `/client/site`,
    label: `Dashboard`
  },
  {
    name: `people`,
    divider: false,
    href: `/client/campus/initiative/people`,
    label: `People`
  },
  {
    name: `stories`,
    divider: false,
    href: `/client/campus/initiative/stories`,
    label: `Stories`
  },
  {
    name: `channels`,
    divider: false,
    href: `/client/campus/initiative/channels`,
    label: `Channels`
  },
  {
    name: `analytics`,
    divider: true,
    href: `/client/campus/initiative/analytics`,
    label: `Analytics`
  },
  {
    name: `apps`,
    divider: false,
    href: `/client/campus/initiative/apps`,
    label: `Apps`
  },
  {
    name: `settings`,
    divider: false,
    href: `/client/campus/initiative/settings`,
    label: `Settings`
  }
]

const Level4 = ({selectedIndex, nav, toggleNav, styles, renderItems}) => (
  <Menu selectedIndex={selectedIndex} collapsed={nav.collapsed}>
    { renderItems(menu) }
    <ContextMenuItem onClick={toggleNav} icon={<ChevronLeftIcon className={css(styles.collapseExpandIcon, nav.collapsed ? styles.iconIsCollapsed : styles.iconIsExpanded)} size={20} />}>Collapse</ContextMenuItem>
  </Menu>
)

Level4.propTypes = {
  selectedIndex: PropTypes.number,
  nav: PropTypes.obj,
  toggleNav: PropTypes.func,
  styles: PropTypes.obj,
  renderItems: PropTypes.func
}

export default Level4

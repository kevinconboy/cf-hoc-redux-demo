import CFComponent from '~/CFComponent'
import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {iconSet} from '~/Icon/IconSet'
import ContextMenuItem from './ContextMenuItem'
import Level1 from './ContextMenus/Level1'
import Level2 from './ContextMenus/Level2'
import Level3 from './ContextMenus/Level3'
import Level4 from './ContextMenus/Level4'

import {toggleNav} from '@/initRedux'

function mapDispatchToProps (dispatch) {
  return bindActionCreators({toggleNav}, dispatch)
}

function mapStateToProps (state) {
  return {nav: state.nav}
}

export function renderMenuItems (items) {
  return items.map((item, i) => (
    <ContextMenuItem iconSet={iconSet(item.name)} key={`context_${i}`} divider={item.divider} href={item.href}>{ item.label }</ContextMenuItem>
    )
  )
}

class ContextMenuComponent extends CFComponent {
  propTypes: {
    title: PropTypes.string,
    collapsed: PropTypes.bool,
    level: PropTypes.number,
    img: PropTypes.string,
    isPopped: PropTypes.bool,
    onPop: PropTypes.func,
    selectedIndex: PropTypes.number,
  }

  toggleNav = () => {
    if (this.props.nav.collapsed) {
      toggleNav(`EXPAND`)
    } else {
      toggleNav(`COLLAPSE`)
    }
  }

  render () {
    const styles = StyleSheet.create({
      ContextMenu: {
        ...t.pt3,
        ...t.overflow_scroll,
        ...t.relative,
        height: `calc( 100vh - 68px )`,
        minWidth: 235,
        fontFamily: `Source Sans Pro`,
        backgroundColor: this.context.colors.grey10,
        transformStyle: `preserve-3d`,
        transition: `min-width 10ms ease-out`
      },
      isCollapsed: {
        maxWidth: 60,
        minWidth: 60,
        width: 60
      },
      UserName: {
        ...t.db,
        color: this.context.colors.white500
      },
      UserTitle: {
        ...t.db,
        ...t.mt2,
        ...t.f4,
        color: this.context.colors.white500_50
      },
      collapseExpandItem: {

      },
      collapseExpandIcon: {
        transition: `all 100ms ease-in-out`
      },
      iconIsCollapsed: {
        transform: `rotate(-180deg)`
      }
    })
    return (
      <div className={css(styles.ContextMenu, styles[`level_${this.props.level}`], this.props.nav.collapsed ? styles.isCollapsed : null)}>
        { !this.props.children && this.props.level === 1 && <Level1 renderItems={renderMenuItems} nav={this.props.nav} selectedIndex={this.props.selectedIndex} toggleNav={this.toggleNav} styles={styles} /> }
        { !this.props.children && this.props.level === 2 && <Level2 renderItems={renderMenuItems} nav={this.props.nav} selectedIndex={this.props.selectedIndex} toggleNav={this.toggleNav} styles={styles} /> }
        { !this.props.children && this.props.level === 3 && <Level3 renderItems={renderMenuItems} nav={this.props.nav} selectedIndex={this.props.selectedIndex} toggleNav={this.toggleNav} styles={styles} /> }
        { !this.props.children && this.props.level === 4 && <Level4 renderItems={renderMenuItems} nav={this.props.nav} selectedIndex={this.props.selectedIndex} toggleNav={this.toggleNav} styles={styles} /> }
        { this.props.children }
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContextMenuComponent)

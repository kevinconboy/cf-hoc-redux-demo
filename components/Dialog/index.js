import PropTypes from 'prop-types'
import Popover from '~/Popover'

const Dialog = props =>
  <Popover
    {...props}
    level={2}
    centered
    size={2}
    isPopped={props.isPopped} >
    { props.children }
  </Popover>

  Dialog.propTypes = {
    isPopped: PropTypes.bool
  }

export default Dialog

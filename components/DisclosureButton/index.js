import PropTypes from 'prop-types'
import CFComponent from '~/CFComponent'

import ChevronRightIcon from 'react-icons/lib/md/chevron-right'
import MoveIcon from 'react-icons/lib/md/drag-handle'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

import Button from '~/Button'

export default class DisclosureButton extends CFComponent {
  propTypes: {
    afterIcon: PropTypes.element,
    collapsed: PropTypes.bool,
    onClick: PropTypes.func,
  }

  render () {
    const styles = StyleSheet.create({
      DisclosureButton: {
        ...t.br__top,
        backgroundColor: this.context.colors.grey5,
        color: this.context.colors.grey300,
        ':hover': {
          backgroundColor: this.context.colors.grey10
        }
      },
      DisclosureButtonIcon: {
        transition: `all 100ms ease-in-out`
      },
      isExpanded: {
        transform: `rotate( 90deg )`
      }
    })

    return (
      <Button
        {...this.props}
        borderless
        afterIcon={<MoveIcon size={15} />}
        className={styles.DisclosureButton}
        block
        intense
        icon={<ChevronRightIcon
          size={20} className={
          css(
            styles.DisclosureButtonIcon,
            !this.props.collapsed && styles.isExpanded
          )
        } />
      }>{ this.props.children }</Button>
    )
  }
}

import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'
import reactElementToJSXString from 'react-element-to-jsx-string'
import SyntaxHighlighter from 'react-syntax-highlighter-prismjs'
import {docco} from 'react-syntax-highlighter-prismjs/dist/styles'

export default class ExampleMatrix extends React.Component {
  renderChildrenAsFlex = styles => (
    this.props.children.map((child, index) => (
      React.cloneElement(child, {key: `elemntChild_${index}`, className: [styles.ExampleComponent, this.props.column && styles.isColumnComponent]})
    ))
  )

  renderChildrenToString = () => {
    let childrenString = ''
    this.props.children.map((child, index) => (
      childrenString += `${reactElementToJSXString(child)}\n\n`
    ))
    return childrenString
  }

  render () {
    const styles = StyleSheet.create({
      ExampleMatrix: {
        ...t.inline_flex,
        ...t.flex_wrap,
        ...t.flex_auto
      },
      isColumn: {
        ...t.flex,
        ...t.flex_column
      },
      ExampleComponent: {
        flex: 'auto',
        ...t.mr3
      },
      isColumnComponent: {
        ...t.mb3
      }
    })

    return (
      <div>
        <div className={css(styles.ExampleMatrix)}>
          {this.renderChildrenAsFlex(styles)}
        </div>
        <SyntaxHighlighter customStyle={{padding: 10}} language="html" style={docco}>{this.renderChildrenToString()}</SyntaxHighlighter>
      </div>
    )
  }
}

ExampleMatrix.propTypes = {
  column: PropTypes.bool
}

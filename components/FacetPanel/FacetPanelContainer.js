import {styles} from './styles'
import {css} from 'aphrodite/no-important'

export default props => <div className={css(styles.FacetPanelContainer)}>{ props.children }</div>

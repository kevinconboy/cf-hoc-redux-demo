import CFComponent from '~/CFComponent'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class extends CFComponent {
  render () {
    const styles = StyleSheet.create({
      FacetPanelHeader: {
        ...t.mb2,
        ...t.pb3,
        ...t.flex,
        ...t.flex_nowrap,
        borderBottom: `1px ${this.context.colors.grey5} solid`
      }
    })

    return (
      <div className={css(styles.FacetPanelHeader)}>{ this.props.children }</div>
    )
  }
}

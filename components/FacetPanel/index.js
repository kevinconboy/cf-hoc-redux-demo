import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import Card from '~/Card'
import DisclosureButton from '~/DisclosureButton'
import {styles} from './styles'

export default class FacetPanel extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      collapsed: true
    }
  }

  onClick = () => {
    if (this.state.collapsed) {
      this.setState({
        collapsed: false
      })
    } else {
      this.setState({
        collapsed: true
      })
    }
  }

  render () {
    return (
      <Card compact className={styles.FacetPanelCard}>
        <DisclosureButton secondary onClick={this.onClick} collapsed={this.state.collapsed}>{ this.props.label }</DisclosureButton>
        <div className={css(styles.FacetPanelContent, this.state.collapsed && styles.isCollapsed)}>
          { this.props.children }
        </div>
      </Card>
    )
  }
}

FacetPanel.propTypes = {
  label: PropTypes.string
}

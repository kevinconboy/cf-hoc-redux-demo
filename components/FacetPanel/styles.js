import {StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  FacetPanelCard: {
    ...t.pa0,
  },
  FacetPanelContent: {
    ...t.pa3
  },
  isCollapsed: {
    ...t.dn
  }

})

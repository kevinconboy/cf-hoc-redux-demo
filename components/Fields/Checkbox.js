import PropTypes from 'prop-types'
import FieldComponent from './FieldComponent'
import {css} from 'aphrodite/no-important'

export default class Checkbox extends FieldComponent {
  constructor (props) {
    super(props)
    this.state = {
      checked: props.defaultChecked || false
    }
  }

  onClick = e => {
    this.setState({
      checked: this.refs.checkbox.checked
    })
    this.props.onClick(e)
  }

  render () {
    const styles = this.getStyles()
    return (
      <input
        ref="checkbox"
        onClick={this.onClick}
        id={this.props.id}
        name={this.props.name}
        className={css(styles.Checkbox)}
        type="checkbox"
        defaultChecked={this.props.defaultChecked} />
    )
  }
}

Checkbox.propTypes = {
  defaultChecked: PropTypes.bool,
  name: PropTypes.string,
  id: PropTypes.string,
  onClick: PropTypes.func,
}

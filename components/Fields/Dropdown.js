import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import FieldComponent from './FieldComponent'
import Button from '~/Button'
import ChevronDownIcon from 'react-icons/lib/md/keyboard-arrow-down'
import Popover from '~/Popover'
import MenuItem from '~/Menu/MenuItem'
import Menu from '~/Menu'

export default class Dropdown extends FieldComponent {
  constructor (props) {
    super(props)
    this.state = {
      selectedIndex: null,
      isDropped: false
    }
  }

  onPop = () => {
    if (this.state.isDropped) {
      this.setState({
        isDropped: false
      })
    } else {
      this.setState({
        isDropped: true
      })
    }
  }

  setSelection = (e, targetProps) => {
    this.setState({
      isDropped: false,
      selectedIndex: targetProps.index,
      selectedText: targetProps.value
    })
  }

  render () {
    const styles = this.getStyles()
    return (
      <span className={css(this.props.block && styles.fieldContainerIsBlock)}>
        <Button
          ref="button"
          onClick={this.onPop}
          compact
          afterIconStyle={{marginRight: 0}}
          afterIcon={<ChevronDownIcon className={css(styles.DropdownIcon, this.state.isDropped && styles.isDropped)} size={20} />}
          block
          labelClassName={this.state.selectedText ? styles.SelectedText : styles.Placeholder}
          className={[ styles.InputField, styles.Dropdown ]}>
          { this.state.selectedText ? this.state.selectedText : this.props.placeholder }
        </Button>

        { this.props.icon && React.cloneElement(this.props.icon, {className: css(styles.InputIcon)})}

        { this.state.isDropped && <Popover
          cardLevel={2}
          level={1}
          target={this.refs.button}
          isPopped={this.state.isDropped}
          onClose={this.onPop}>
          <Menu compact selectedIndex={this.state.selectedIndex}>
            <MenuItem onClick={this.setSelection}>Lorem ipsum dolor</MenuItem>
            <MenuItem onClick={this.setSelection}>Sit amet</MenuItem>
          </Menu>
        </Popover>}
      </span>
    )
  }
}

Dropdown.propTypes = {
  defaultSelected: PropTypes.bool,
  placeholder: PropTypes.string,
  menuItems: PropTypes.array,
  block: PropTypes.bool
}

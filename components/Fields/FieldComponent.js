import CFComponent from '~/CFComponent'

import {StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class FieldComponent extends CFComponent {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  getStyles () {
    return StyleSheet.create({
      InputFieldContainer: {
        ...t.relative,
        transition: `100ms ease-in-out all`
      },
      InputField: {
        ...t.pa1,
        ...t.f3,
        ...t.outline_0,
        ...t.lh_copy,
        transition: `all 100ms ease-in-out`,
        color: this.context.colors.blueGrey500,
        border: `1px solid ${this.context.colors.grey10}`,
        fontFamily: `Source Sans Pro`,
        ':focus': {
          border: `1px solid ${this.context.colors.primary500}`
        },
        '::placeholder': {
          color: this.context.colors.blueGrey10
        }
      },
      inputFieldHasError: {
        border: `1px solid ${this.context.colors.alert100}`,
      },
      hasIcon: {
        ...t.pl5
      },
      isBlock: {
        ...t.flex_column,
        ...t.items_start
      },
      fieldContainerIsBlock: {
        ...t.db,
        ...t.w_100
      },
      fieldIsBlock: {
        ...t.db,
        width: `calc( 100% - 10px )`
      },
      InputIcon: {
        ...t.absolute,
        left: 6,
        top: 7,
        color: this.context.colors.blueGrey10
      },
      DropdownIcon: {
        transition: `all ease-in-out 100ms`
      },
      FieldLabel: {
        color: this.context.colors.grey100,
        ...t.f3,
        ...t.db,
        flex: 100
      },
      fieldLabelHasError: {
        color: this.context.colors.alert100,
      },
      FieldSet: {
        ...t.bn,
        ...t.pa0,
      },
      Dropdown: {
        maxHeight: `inherit`,
        color: this.context.colors.grey100
      },
      isDropped: {
        ...t.relative,
        transform: `rotate( -180deg )`,
        transformOrigin: `center center`
      },
      Placeholder: {
        color: this.context.colors.blueGrey10
      },
      FieldGroup: {
        ...t.pb2,
        ...t.pt2,
        borderBottom: `1px solid ${this.context.colors.grey10}`,
        ...t.flex,
        ...t.items_center,
        ':last-of-type': {
          ...t.bn
        }
      },
      fieldGroupHasError: {

      },
      SelectedText: {
        color: this.context.colors.button500
      },
      Checkbox: {
        ...t.mr2
      },
      isChecked: {
        color: this.context.colors.primary800
      }
    })
  }

  render () {
    return (
      <div>{ this.props.children }</div>
    )
  }
}

import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import TextField from './TextField'
import FieldLabel from './FieldLabel'
import Dropdown from './Dropdown'
import Checkbox from './Checkbox'
import Password from './Password'
import FieldComponent from './FieldComponent'
import SystemMessage from '~/SystemMessage'

export default class FieldGroup extends FieldComponent {
  onCheckBoxboxToggle = e => {
    this.setState({
      checked: e.target.checked
    })
  }

  render () {
    const styles = this.getStyles()

    if (this.props.children) {
      return (
        <div className={css(styles.FieldGroup)}>
          { this.props.children }
        </div>
      )
    } else {
      return (
        <div className={css(
          styles.FieldGroup,
          this.props.error && styles.fieldGroupHasError,
          this.props.kind === `TextField` && styles.isBlock,
          this.props.kind === `Password` && styles.isBlock,
          this.props.kind === `Dropdown` && styles.isBlock)}>
          { this.props.kind === `Checkbox` && <Checkbox onClick={this.onCheckBoxboxToggle} defaultChecked={this.props.defaultChecked} {...this.props} /> }
          { this.props.label && <FieldLabel checked={this.state.checked} {...this.props} htmlFor={this.props.id}>{ this.props.label }</FieldLabel> }
          { this.props.kind === `Password` && <Password block {...this.props} /> }
          { this.props.kind === `TextField` && <TextField block {...this.props} /> }
          { this.props.kind === `Dropdown` && <Dropdown block {...this.props} /> }
          { this.props.error && <SystemMessage error inline block message={this.props.error} /> }
        </div>
      )
    }
  }
}

FieldGroup.propTypes = {
  label: PropTypes.string,
  kind: PropTypes.oneOf([`TextField`, `Checkbox`, `Dropdown`, `Password`]),
  error: PropTypes.string
}

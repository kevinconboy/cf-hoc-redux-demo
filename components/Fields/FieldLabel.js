import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import FieldComponent from './FieldComponent'

export default class FieldLabel extends FieldComponent {
  render () {
    const styles = this.getStyles()

    return (
      <label
        htmlFor={this.props.htmlFor}
        className={css(
          styles.FieldLabel,
          this.props.checked && styles.isChecked,
          this.props.error && styles.fieldLabelHasError
        )}>
        { this.props.children }
      </label>
    )
  }
}

FieldComponent.propTypes = {
  htmlFor: PropTypes.string,
  checked: PropTypes.bool,
  error: PropTypes.string
}

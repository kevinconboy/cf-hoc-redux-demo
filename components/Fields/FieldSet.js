import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import FieldComponent from './FieldComponent'

export default class FieldSet extends FieldComponent {
  render () {
    const styles = this.getStyles()
    return (
      <fieldset className={css(styles.FieldSet)}>
        {this.props.message}
        { this.props.children}
      </fieldset>
    )
  }
}

FieldSet.propTypes = {
  message: PropTypes.element
}

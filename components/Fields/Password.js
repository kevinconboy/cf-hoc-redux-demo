import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import FieldComponent from './FieldComponent'

export default class Password extends FieldComponent {
  constructor (props) {
    super(props)

    this.state.value = null
  }

  onChange = e => this.setState({value: this.refs.Password.value})

  onBlur = e => this.props.onBlur && this.props.onBlur(e)

  onClick = e => this.props.onClick && this.props.onClick(e)

  onFocus = e => this.props.onFocus && this.props.onFocus(e)

  render () {
    const styles = this.getStyles()
    return (
      <span className={css(styles.InputFieldContainer, this.props.block && styles.fieldContainerIsBlock)}>
        <input
          onClick={this.onClick}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          onChange={this.setValue}
          ref="Password"
          className={css(
            styles.InputField,
            this.props.icon && styles.hasIcon,
            this.props.block && styles.fieldIsBlock,
            this.props.error && styles.inputFieldHasError,
            this.props.className)}
          type="password" />
        { this.props.icon && React.cloneElement(this.props.icon, {className: css(styles.InputIcon)})}
      </span>
    )
  }
}

Password.propTypes = {
  icon: PropTypes.element,
  block: PropTypes.bool,
  onBlur: PropTypes.func,
  onClick: PropTypes.func,
  onFocus: PropTypes.func,
  error: PropTypes.string
}

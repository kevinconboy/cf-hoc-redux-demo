import TextField from '~/Fields/TextField'
import SearchIcon from 'react-icons/lib/md/search'
import PropTypes from 'prop-types'
import {StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

const styles = StyleSheet.create({
  SearchField: {
    ...t.br_pill,
    width: 400
  }
})

const SearchField = props =>
  <TextField
    className={styles.SearchField}
    icon={<SearchIcon size={20} />}
    placeholder={props.placeholder} />

SearchField.propTypes = {
  placeholder: PropTypes.string
}

export default SearchField

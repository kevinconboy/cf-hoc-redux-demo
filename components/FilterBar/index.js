import Button from '~/Button'
import AddIcon from 'react-icons/lib/md/add'

export default () => <Button icon={<AddIcon size={16} />} compact weighted secondary block>Advanced Filters</Button>

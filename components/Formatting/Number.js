import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default props => {
  const styles = StyleSheet.create({
    Number: {
      ...t.tr,
      ...t.db
    }
  })

  return (
    <span className={css(styles.Number)}>{ props.children }</span>
  )
}

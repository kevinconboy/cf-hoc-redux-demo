import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import UtilityBar from '~/UtilityBar'

import {styles} from './styles'
import ChevronLeftIcon from 'react-icons/lib/md/chevron-left'
import Button from '~/Button'

const Header = props =>
  <header className={
    css(
      styles.Header,
      props.collapsed && styles.isCollapsed,
      props.compact && styles.isCompact,
      props.divider && styles.hasDivider,
      styles[ `level${props.level}` ],
      props.weighted && styles.isWeighted,
      props.back && styles.hasBackButton,
      props.className,
      props.selectedClassName
     )}>
    { props.back && <Button borderless compact><ChevronLeftIcon size={30} /></Button> }
    <span>{ props.children }</span>
    { props.utilityComponent && <UtilityBar>{ props.utilityComponent }</UtilityBar>}
  </header>

Header.propTypes = {
  level: PropTypes.number,
  divider: PropTypes.bool,
  weighted: PropTypes.bool,
  compact: PropTypes.bool,
  selectedClassName: PropTypes.object,
  utilityComponent: PropTypes.func,
  back: PropTypes.bool,
  collapsed: PropTypes.bool
}

export default Header

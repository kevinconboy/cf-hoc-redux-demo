import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  Header: {
    ...t.f3,
    ...t.pt3,
    ...t.pb1,
    ...t.ma0,
    ...t.flex,
    ...t.content_center,
    ...t.flex
  },
  hasDivider: {
    ...t.mb3,
    ...t.pb3,
    borderBottomColor: colors.grey10,
    borderBottomStyle: `solid`,
    borderBottomWidth: 1
  },
  backButton: {

  },
  isCompact: {
    ...t.pa0,
    ...t.ma0
  },
  isWeighted: {
    ...t.mb3
  },
  level0: {
    ...t.f1,
    ...t.pt0,
    ...t.normal,
    color: colors.blueGrey800
  },
  level1: {
    ...t.f2
  },
  level2: {
    ...t.f3,
    ...t.pt1,
    ...t.pb1,
    ...t.normal
  },
  level3: {
    ...t.f3,
    ...t.fw6
  },
  level4: {
    ...t.f4,
    ...t.ttu,
    ...t.normal
  },
  level5: {
    ...t.f6,
    ...t.ttu,
    color: colors.blueGrey100
  }
})

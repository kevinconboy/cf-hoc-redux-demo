import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class IconBar extends React.Component {
  renderChildrenWithClassNames = styles => {
    const childrenWithClassNames = []

    for (let i = 0; i < this.props.children.length; i++) {
      let className
      if (i === 0) {
        className = styles.isFirst
      } else if (i === this.props.children.length - 1) {
        className = styles.isLast
      } else {
        className = styles.isMiddle
      }

      childrenWithClassNames.push(React.cloneElement(this.props.children[ i ], {key: i, className: className}))
    }
    return childrenWithClassNames
  }

  render () {
    const styles = StyleSheet.create({
      isFirst: {
        ...t.br__left
      },
      isMiddle: {
        ...t.br0,
        borderRightWidth: 0
      },
      isLast: {
        borderLeftWidth: 0,
        ...t.br__right
      },
      IconBar: {
        ...t.ml2,
        ...t.nowrap
      }
    })

    return (
      <div className={css(styles.IconBar)}>{ this.renderChildrenWithClassNames(styles) }</div>
    )
  }
}

import Icon from './'

export function iconSet (name) {
  const names = {
    on: `${name}_on`,
    off: `${name}_off`
  }
  return {
    default: <Icon kind={names.off} size={20} />,
    selected: <Icon kind={names.on} size={20} />
  }
}

import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import {styles} from './styles'

const Logo = props => {
  if (props.white) {
    return <img src="/static/img/logo-white.png" className={css(props.isAppIcon ? styles.isAppIcon : null)} style={{width: props.size, height: props.size * 0.84}} />
  } else {
    return <img src="/static/img/logo-green.png" className={css(props.isAppIcon ? styles.isAppIcon : null)} style={{width: props.size, height: props.size * 0.84}} />
  }
}

Logo.defaultProps = {
  size: 32
}

Logo.propTypes = {
  size: PropTypes.number,
  white: PropTypes.bool,
  isAppIcon: PropTypes.bool
}

export default Logo

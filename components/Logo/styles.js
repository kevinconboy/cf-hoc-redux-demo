import {StyleSheet} from 'aphrodite/no-important'

import t from 'tachyons-js'

export const styles = StyleSheet.create({
  isAppIcon: {
    '@media( max-width: 800px )': {
      ...t.dn
    }
  }

})

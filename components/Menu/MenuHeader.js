
import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

import Header from '~/Header'
import Avatar from '~/Avatar'
import MenuHeaderDrop from '~/Menu/MenuHeaderDrop'

const MenuHeader = props =>
  <div className={css(styles.MenuHeader, props.hasDrop ? styles.hasDrop : null)}>
    { props.img
      ? <Avatar size={30} img={props.img} />
      : null
    }
    <Header level={1}>{ props.children }</Header>
    { props.hasDrop
      ? <MenuHeaderDrop />
    : null
    }
  </div>

MenuHeader.propTypes = {
  img: PropTypes.string,
  hasDrop: PropTypes.string
}

export default MenuHeader

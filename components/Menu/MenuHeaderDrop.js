
import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

import Button from '~/Button'
import ArrowDownIcon from 'react-icons/lib/md/keyboard-arrow-down'

const MenuHeaderDrop = props =>
  <div className={css(styles.MenuHeaderDrop)}>
    <Button className={props.isDropped ? styles.isDropped : null} icon={<ArrowDownIcon size={20} />} borderless inverted />
  </div>

MenuHeaderDrop.propTypes = {
  isDropped: PropTypes.bool
}

export default MenuHeaderDrop

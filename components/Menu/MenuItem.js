import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

import Button from '~/Button'

export default class MenuItem extends React.Component {
  renderIcon = () => {
    if (this.props.iconSet) {
      if (this.props.selected) {
        return this.props.iconSet.selected
      } else {
        return this.props.iconSet.default
      }
    } else {
      return this.props.icon
    }
  }

  onClick = e => this.props.onClick && this.props.onClick(e, this.props)

  render () {
    return (
      <li className={css(
          styles.MenuItem,
          this.props.selected && styles.isSelected,
          this.props.divider && styles.isDivider,
          this.props.className,
          this.props.beforeSelected && styles.isBeforeSelected
        )}>
        <Button
          collapsed={this.props.collapsed}
          onClick={this.onClick}
          prefetch
          href={this.props.href}
          borderless
          inverted
          block
          icon={this.renderIcon()}>
          { this.props.children }
        </Button>
      </li>
    )
  }
}

MenuItem.propTypes = {
  icon: PropTypes.element,
  iconSet: PropTypes.object,
  selected: PropTypes.bool,
  beforeSelected: PropTypes.bool,
  href: PropTypes.string,
  divider: PropTypes.bool,
  onClick: PropTypes.func,
  collapsed: PropTypes.bool
  }

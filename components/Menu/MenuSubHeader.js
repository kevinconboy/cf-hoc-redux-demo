
import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

import Header from '~/Header'
import Avatar from '~/Avatar'
import MenuHeaderDrop from '~/Menu/MenuHeaderDrop'

export default class MenuSubHeader extends React.Component {
  onClick = e => {
    if (this.props.hasDrop) {
      this.props.onClick(e)
    }
  }

  render () {
    return (
      <div className={css(styles.MenuSubHeader, this.props.hasDrop ? styles.hasDrop : null, this.props.menuIsDropped ? styles.isDropped : null)} onClick={this.onClick}>
        { this.props.img
          ? <Avatar size={30} img={this.props.img} />
          : null
        }
        <Header level={1}>{ this.props.children }</Header>
        { this.props.hasDrop
            ? <MenuHeaderDrop isDropped={this.props.isDropped} />
          : null
          }

      </div>
    )
  }
}

MenuSubHeader.propTypes = {
  img: PropTypes.string,
  hasDrop: PropTypes.bool,
  isDropped: PropTypes.bool,
  onClick: PropTypes.func,
  menuIsDropped: PropTypes.bool
}

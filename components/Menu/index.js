
import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

export default class Menu extends React.Component {
  renderChildrenWithSelected = () => {
    return React.Children.map(this.props.children, (child, index) => (
      React.cloneElement(
        child, {
          index: index,
          key: index,
          value: child.props.children,
          beforeSelected: this.props.selectedIndex === index + 2,
          selected: this.props.selectedIndex === index + 1,
          collapsed: this.props.collapsed}
        )
      )
    )
  }

  render () {
    return (
      <ul className={css(styles.Menu)}>
        { this.renderChildrenWithSelected() }
      </ul>
    )
  }
}

Menu.propTypes = {
  selectedIndex: PropTypes.number,
  collapsed: PropTypes.bool
}

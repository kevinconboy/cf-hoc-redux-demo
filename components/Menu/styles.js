import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  Menu: {
    ...t.list,
    ...t.pa0,
    ...t.ma0
  },
  MenuItem: {
    ...t.f3,
    ...t.nested_list_reset,
    ...t.lh_title,
    color: colors.grey800,
    borderBottomWidth: 1,
    borderBottomColor: colors.grey500_25,
    borderBottomStyle: `solid`,
    ':hover': {
      color: colors.grey1000
    },
    ':last-of-type': {
      borderBottomWidth: 0
    }
  },
  isBeforeSelected: {
    borderBottomColor: `rgba(255,255,255,0)`
  },
  isSelected: {
    color: colors.grey800,
    backgroundColor: colors.white500,
    borderRadius: 30
  },
  isDivider: {
    ...t.mb4,
    borderBottomWidth: 0
  },
  MenuHeader: {
    ...t.flex,
    ...t.items_center,
    ...t.lh_title,
    ...t.pl3,
    ...t.pr3,
    color: colors.white500,
    height: 87,
    backgroundColor: colors.blueGrey1000
  },
  hasDrop: {
    ...t.pointer
  },
  MenuSubHeader: {
    ...t.flex,
    ...t.items_center,
    ...t.lh_title,
    ...t.pl3,
    ...t.pr3,
    borderTopWidth: 1,
    borderTopColor: colors.white500_25,
    borderTopStyle: `solid`,
    height: 87,
    color: colors.blueGrey10,
    backgroundColor: colors.blueGrey950
  },
  MenuHeaderDrop: {
    color: colors.white500,
    width: 40
  },
  isDropped: {
    ...t.relative,
    transform: `rotate( -180deg )`,
    transformOrigin: 32
  }
})

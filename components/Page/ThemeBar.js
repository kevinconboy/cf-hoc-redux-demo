import CFComponent from '~/CFComponent'
import {StyleSheet, css} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class ThemeBar extends CFComponent {
  render () {
    const styles = StyleSheet.create({
      ThemeBar: {
        ...t.fixed,
        ...t.w_100,
        height: 2.5,
        top: 50
      }
    })

    return (
      <div className={css(styles.ThemeBar)} style={{background: `-webkit-linear-gradient(left, ${this.context.colors.primary500}, ${this.context.colors.secondary500} )`}} />
    )
  }
}

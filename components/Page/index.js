import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {css} from 'aphrodite/no-important'

import {toggleNav, setNavLevel, toggleDialog, handleLogin} from '@/initRedux'
import {styles} from './styles'
import {calculateColors} from '^'

import AppBar from '~/AppBar'
import ContextMenu from '~/ContextMenu'
import ScrollContent from '~/ScrollContent'
import ThemeBar from './ThemeBar'

function mapDispatchToProps (dispatch) {
  return bindActionCreators({toggleNav, setNavLevel, toggleDialog, handleLogin}, dispatch)
}

function mapStateToProps (state) {
  return {nav: state.nav, user: state.user, colors: state.colors, dialog: state.dialog, auth: state.auth}
}

class PageComponent extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      width: 0
    }
  }

  getInitialProps = () => {
    setNavLevel(this.props.level)
    if (ReactDOM.findDOMNode(this.refs.PageWrapper).getBoundingClientRect().width < 800) {
      toggleNav(`COLLAPSE`)
    }
  }

  componentDidMount = () => {
    if (this.props.level !== this.props.nav.level) {
      setNavLevel(this.props.level)
    }
    if (ReactDOM.findDOMNode(this.refs.PageWrapper).getBoundingClientRect().width < 800) {
      toggleNav(`COLLAPSE`)
    }
    window.addEventListener(`resize`, this.updateDimensions)
  }

  componentWillUnmount = () => {
    window.removeEventListener(`resize`, this.updateDimensions)
  }

  updateDimensions = () => {
    const width = ReactDOM.findDOMNode(this.refs.PageWrapper).getBoundingClientRect().width
    this.setState({
      width: width
    })
    if (width < 800 && this.props.nav.collapsed === false) {
      toggleNav(`COLLAPSE`)
    }
  }

  static childContextTypes = {
    colors: PropTypes.object
  }

  getChildContext = () => {
    return {colors: calculateColors(this.props.colors[ this.props.level ])}
  }

  render () {
    return (
      <div className={css(styles.Page)} ref="PageWrapper">

        <AppBar
          title={this.props.title}
          level={this.props.level}
          user={this.props.user}
          icon={this.props.nav.icon}
          captured={this.props.captured}
          sidebarCollapsed={this.props.nav.collapsed} />
        <ThemeBar />
        <div className={css(styles.PageChildren)}>
          { (this.props.level !== 0 || this.props.customMenu) &&
            <ContextMenu
              level={this.props.level}
              selectedIndex={this.props.selectedIndex}
              title={this.props.nav.title}>
              { this.props.customMenu }
            </ContextMenu>
          }
          <ScrollContent narrow={this.props.narrow} captured={this.props.captured}>
            { this.props.children }
          </ScrollContent>
        </div>
      </div>
    )
  }
}

PageComponent.propTypes = {
  level: PropTypes.number,
  selectedIndex: PropTypes.number,
  customMenu: PropTypes.element,
  title: PropTypes.string,
  narrow: PropTypes.bool,
  captured: PropTypes.bool,
  colors: PropTypes.obj,
  user: PropTypes.obj,
  nav: PropTypes.shape({
    collapsed: PropTypes.bool,
    level: PropTypes.number,
    icon: PropTypes.string,
    title: PropTypes.string}),

}

export default connect(mapStateToProps, mapDispatchToProps)(PageComponent)

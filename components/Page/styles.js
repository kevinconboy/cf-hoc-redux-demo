import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  Page: {
    ...t.ma0,
    ...t.pa0,
    fontFamily: `Source Sans Pro`,
    height: `100vh`,
    backgroundColor: colors.white500,
    color: colors.black
  },
  PageChildren: {
    ...t.overflow_hidden,
    ...t.relative,
    ...t.flex,
    backgroundColor: colors.white500,
    top: 52.5,
    height: `calc( 100vh - 52.5px )`,
    width: `100vw`
  }
})

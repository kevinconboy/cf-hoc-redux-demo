import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import {css} from 'aphrodite/no-important'
import Transition from 'react-motion-ui-pack'
import {spring} from 'react-motion'
import Card from '~/Card'

import {styles} from './styles'

export default class Popover extends React.Component {
  onClose = e => {
    window.removeEventListener(`resize`, this.updateDimensions)
    e.stopPropagation()
    this.props.onClose(e)
  }

  constructor (props) {
    super(props)

    this.state = {
      x: 0,
      y: 0
    }
  }

  updateDimensions = () => {
    const rect = ReactDOM.findDOMNode(this.props.target).getBoundingClientRect()

    let x = 0
    let y = 0

    if (this.props.hAlign === `left`) {
      x = rect.x
    } else if (this.props.hAlign === `right`) {
      x = rect.right - 400
    }

    if (this.props.vAlign === `bottom`) {
      y = rect.bottom
    } else if (this.props.vAlign === `top`) {
      y = rect.top
    }

    this.setState({
      x: x,
      y: y
    })
  }

  componentDidMount = () => {
    if (this.props.target) {
      this.updateDimensions()
    }
  }

  cardClick = e => {
    e.stopPropagation()
  }

  render () {
    if (this.props.isPopped) {
      window.addEventListener(`resize`, this.updateDimensions)
    }

    return (
      <Transition
        component={false}
        enter={{
          opacity: spring(1, {stiffness: 700, damping: 50}),
          translateY: spring(0, {stiffness: 700, damping: 50})
        }}
        leave={{
          opacity: 0,
          translateY: -10
        }}>

        { this.props.isPopped &&
          <div key="popover" className={css(styles.PopoverBackdrop, this.props.centered && styles.PopoverBackdropIsCentered)} onClick={this.onClose}>
            <div ref="popoverContent" style={{left: this.state.x, top: this.state.y}} className={css(styles.Popover, this.props.centered && styles.PopoverIsCentered, styles[ `level_${this.props.level}` ])}>
              <Card level={this.props.cardLevel} className={this.props.cardClassName} compact={this.props.compact} onClick={this.cardClick}>{ this.props.children }</Card>
            </div>
          </div>
        }
      </Transition>
    )
  }
}

Popover.propTypes = {
  isPopped: PropTypes.bool,
  target: PropTypes.element,
  onClose: PropTypes.func,
  hAlign: PropTypes.string,
  vAlign: PropTypes.string,
  cardLevel: PropTypes.number,
  level: PropTypes.number,
  cardClassName: PropTypes.string,
  centered: PropTypes.bool,
  compact: PropTypes.bool
}

Popover.defaultProps = {
  hAlign: `left`,
  vAlign: `bottom`
}

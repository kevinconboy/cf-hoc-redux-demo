import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  Popover: {
    ...t.absolute,
    zIndex: 999,
    boxShadow: `0px 1px 2px ${colors.blueGrey10}`
  },
  level_1: {
    width: 300,
    minWidth: 300
  },
  level_2: {
    width: 400,
    minWidth: 400
  },
  PopoverBackdrop: {
    ...t.fixed,
    ...t.absolute__fill,
    zIndex: 1000
  },
  PopoverBackdropIsCentered: {
    ...t.flex,
    ...t.items_center,
    ...t.justify_center,
    backgroundColor: colors.blueGrey500_50
  },
  PopoverIsCentered: {
    position: 'auto'
  }
})

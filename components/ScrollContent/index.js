
import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import {styles} from './styles'

const ScrollContent = props =>
  <div className={css(
    styles.ScrollContent,
    props.fullBleed && styles.isFullBleed,
    props.narrow && styles.isNarrow,
    props.captured && styles.isCaptured)}>
    { props.children }
  </div>

ScrollContent.propTypes = {
  fullBleed: PropTypes.bool,
  narrow: PropTypes.bool,
  captured: PropTypes.bool
}

export default ScrollContent

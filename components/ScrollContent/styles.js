import {StyleSheet} from 'aphrodite/no-important'

import {colors} from '^'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  ScrollContent: {
    ...t.pa4,
    ...t.pt3,
    ...t.overflow_auto,
    ...t.w_100,
    backgroundColor: colors.white_500,
    transformStyle: `preserve-3d`,
    transition: `all 100ms ease-in-out`
  },
  isNarrow: {
    width: `70vw`,
    margin: `0 auto`
  },
  isCaptured: {
    width: 400,
    margin: `0 auto`,
    ...t.flex,
    ...t.justify_center,
    ...t.items_center,
  },
  isFullBleed: {
    ...t.pa0
  }
})

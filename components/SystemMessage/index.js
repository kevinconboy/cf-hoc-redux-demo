import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'
import Header from '~/Header'

import Card from '~/Card'
import {colors} from '^'

const SystemMessage = props => {
  const styles = StyleSheet.create({
    isConfirm: {
      backgroundColor: colors.confirm10,
      ...t.pa2,
      ...t.br3,
      ...t.f4,
      color: colors.confirm800,
      borderColor: colors.confirm500
    },
    isWarning: {
      backgroundColor: colors.warning10,
      ...t.pa2,
      ...t.br3,
      ...t.f4,
      color: colors.warning800,
      borderColor: colors.warning500
    },
    isError: {
      backgroundColor: colors.alert10,
      ...t.pa2,
      ...t.br3,
      ...t.f4,
      color: colors.alert800,
      borderColor: colors.alert50
    },
    isInline: {
      ...t.br0,
      marginTop: '-3px',
      ...t.pt1,
      ...t.pb1,
      ...t.f5,
    },
    cardAlignment: {
      ...t.flex
    },
    cardIconContainer: {
      ...t.dib,
      ...t.mr2,
    }
  })

  return (
    <Card {...props} compact weighted={!props.inline} className={[
      props.confirm && styles.isConfirm,
      props.warning && styles.isWarning,
      props.error && styles.isError,
      props.inline && styles.isInline,
      props.floating && styles.isFloating]}>

      {props.icon
        ? <div className={css(styles.cardAlignment)}>
          <span className={css(styles.cardIconContainer)}>{props.icon}</span>
          <div>
            {props.title && <Header level={4} compact>{props.title}</Header>}
            {props.message}
          </div>
        </div>
        : <div>
          {props.title && <Header level={4} compact>{props.title}</Header>}
          {props.message}
        </div>
      }

    </Card>
  )
}

SystemMessage.propTypes = {
  message: PropTypes.string,
  icon: PropTypes.element,
  title: PropTypes.string,
  warning: PropTypes.bool,
  confirm: PropTypes.bool,
  error: PropTypes.bool,
  floating: PropTypes.bool,
  inline: PropTypes.bool
}

export default SystemMessage

import CFComponent from '~/CFComponent'
import PropTypes from 'prop-types'
import Button from '~/Button'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class Tab extends CFComponent {
  propTypes: {
    index: PropTypes.number,
    onClick: PropTypes.func,
    title: PropTypes.string,
  }

  setSelectedIndex = () => {
    this.props.onClick(this.props.index)
  }

  render = () => {
    const styles = StyleSheet.create({
      Tab: {
        ...t.list,
        ...t.mr4,
        color: this.context.colors.blueGrey50,
        borderBottom: `1px transparent solid`
      },
      TabButtonLabel: {
        ...t.tc,
        ...t.f3,
        ...t.fw4,
        fontFamily: `Source Sans Pro`
      },
      selectedTabButtonLabel: {
        ...t.tc,
        ...t.f3,
        ...t.fw6,
        fontFamily: `Source Sans Pro`
      },
      isSelected: {
        borderWidth: 2,
        borderColor: this.context.colors.primary500,
        color: this.context.colors.blueGrey900
      }
    })

    return (
      <li className={css(styles.Tab, this.props.selected && styles.isSelected)} onClick={this.setSelectedIndex}>
        <Button labelClassName={this.props.selected ? styles.selectedTabButtonLabel : styles.TabButtonLabel} alignCenter borderless block>{ this.props.title }</Button>
      </li>
    )
  }
}

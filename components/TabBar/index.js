import PropTypes from 'prop-types'
import CFComponent from '~/CFComponent'
import {css, StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

import UtilityBar from '~/UtilityBar'

export default class TabBar extends CFComponent {
  constructor (props) {
    super(props)

    if (!props.selectedIndex) {
      this.state = {
        selectedIndex: 1
      }
    } else {
      this.state = {
        selectedIndex: props.selectedIndex
      }
    }
  }

  setSelectedIndex = index => {
    this.setState({
      selectedIndex: index
    })
  }

  renderChildrenWithSelected = () => {
    return React.Children.map(this.props.children, (child, index) => (
      React.cloneElement(child, {
        key: index,
        index: index + 1,
        selected: this.state.selectedIndex === index + 1,
        onClick: this.setSelectedIndex})
      )
    )
  }

  renderSelectedTabContent = () => {
    return React.Children.map(this.props.children, (child, index) => {
      if (this.state.selectedIndex === index + 1) {
        return child.props.displayComponent
      } else return null
    })
  }

  renderUtilityComponent = () => {
    if (this.props.children[ this.state.selectedIndex - 1 ].props.utilityComponent) {
      return this.props.children[ this.state.selectedIndex - 1 ].props.utilityComponent
    } else {
      return this.props.utilityComponent
    }
  }

  render () {
    const styles = StyleSheet.create({
      Tabs: {
        ...t.pa0,
        ...t.ma0,
        ...t.flex,
        ...t.mb4,
        borderBottom: `1px ${this.context.colors.grey10} solid`
      }
    })

    return (
      <div className="TabView">
        <nav className="TabBar">
          <ul className={css(styles.Tabs)}>
            { this.renderChildrenWithSelected() }
            <UtilityBar>{ this.renderUtilityComponent() }</UtilityBar>
          </ul>
        </nav>
        <div className="TabContent">
          { this.renderSelectedTabContent() }
        </div>
      </div>
    )
  }
}

TabBar.propTypes = {
  utilityComponent: PropTypes.element
}

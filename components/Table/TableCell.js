import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'

import TableComponent from './TableComponent'

export default class TableRow extends TableComponent {
  render () {
    const styles = this.getStyles()
    return (
      <td className={css(styles.TableCell, this.props.thead && styles.TableCellTHead, this.props.alignRight && styles.isAlignedRight)}>
        { this.props.children }
      </td>
    )
  }
}

TableRow.propTypes = {
  thead: PropTypes.bool,
  alignRight: PropTypes.bool,
}

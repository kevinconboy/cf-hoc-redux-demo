import PropTypes from 'prop-types'
import TableComponent from './TableComponent'
import TableCell from './TableCell'
import Button from '~/Button'

export default class TableColumnHeaders extends TableComponent {
  render () {
    const styles = this.getStyles()

    return (
      <thead>
        <tr>
          { this.props.columns && this.props.columns.map((column, index) => <TableCell thead key={`cell_${index}`}><Button tertiary block compact borderless inline labelClassName={styles.THeadButtonLabel}>{ column }</Button></TableCell>) }
        </tr>
      </thead>
    )
  }
}

TableColumnHeaders.propTypes = {
  columns: PropTypes.array,
}

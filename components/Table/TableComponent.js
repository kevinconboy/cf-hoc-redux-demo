import CFComponent from '~/CFComponent'

import {StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export default class TableComponent extends CFComponent {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  getStyles = () => {
    return StyleSheet.create({
      TableHeader: {
        ...t.flex
      },
      Table: {
        ...t.w_100,
        ...t.collapse,
        borderTop: `1px ${this.context.colors.grey10} solid`,
        ...t.mt3
      },
      TableCell: {
        borderBottom: `1px ${this.context.colors.grey10} solid`,
        color: this.context.colors.grey900,
        ...t.pt2,
        ...t.pb2,
        ...t.f3
      },
      THeadButtonLabel: {
        ...t.pl0
      },
      TableCellTHead: {
        ...t.pt3,
        ...t.pb3
      },
      isAlignedRight: {
        ...t.tar
      }
    })
  }

  render () {
    return (
      <div>{ this.props.children }</div>
    )
  }
}

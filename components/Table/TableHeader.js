import PropTypes from 'prop-types'
import UtilityBar from '~/UtilityBar'
import TableComponent from './TableComponent'
import {css} from 'aphrodite/no-important'

export default class TableHeader extends TableComponent {
  render () {
    const styles = this.getStyles()

    return (
      <div className={css(styles.TableHeader)}>
        { this.props.actionComponent }
        <UtilityBar>
          { this.props.utilityComponent }
        </UtilityBar>
      </div>
    )
  }
}

TableHeader.propTypes = {
  utilityComponent: PropTypes.element,
  actionComponent: PropTypes.element,
}

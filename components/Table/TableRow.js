import {css} from 'aphrodite/no-important'

import TableComponent from './TableComponent'
import TableCell from './TableCell'

export default class TableRow extends TableComponent {
  render () {
    const styles = this.getStyles()

    return (
      <tr className={css(styles.TableRow)}>
        { this.props.children.map((child, index) => <TableCell key={index}>{ child }</TableCell>) }
      </tr>
    )
  }
}

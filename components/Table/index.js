import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import TableComponent from './TableComponent'
import Card from '~/Card'

export default class Table extends TableComponent {
  render () {
    const styles = this.getStyles()

    return (
      <Card level={2}>
        { this.props.header }
        <table className={css(styles.Table)}>
          { this.props.columnHeaders }
          <tbody>
            { this.props.children }
          </tbody>
        </table>
        { this.props.footer }
      </Card>
    )
  }
}

Table.propTypes = {
  header: PropTypes.element,
  columnHeaders: PropTypes.element,
}

import PropTypes from 'prop-types'
import {css} from 'aphrodite/no-important'
import {styles} from './styles'

export default class UtilityBar extends React.Component {
  renderChildrenWithClassNames = () => {
    if (this.props.children.length > 1 && !this.props.compact) {
      const childrenWithClassNames = []

      for (let i = 0; i < this.props.children.length; i++) {
        let className
        if (i === 0) {
          className = styles.isFirst
        } else if (i === this.props.children.length - 1) {
          className = styles.isLast
        } else {
          className = styles.isMiddle
        }

        childrenWithClassNames.push(React.cloneElement(this.props.children[ i ], {key: i, className: className}))
      }
      return childrenWithClassNames
    } else {
      return this.props.children
    }
  }

  render () {
    return (
      <div className={css(styles.UtilityBar)}>{ this.props.children && this.renderChildrenWithClassNames() }</div>
    )
  }
}

UtilityBar.propTypes = {
  compact: PropTypes.bool
}

import {StyleSheet} from 'aphrodite/no-important'
import t from 'tachyons-js'

export const styles = StyleSheet.create({
  UtilityBar: {
    ...t.tar,
    ...t.flex,
    ...t.justify_end,
    ...t.w_100,
    flex: 100,
    ...t.w_100,
    ...t.content_center,
    ...t.items_center,
    maxHeight: 35
  },
  isMiddle: {
    ...t.ml2
  },
  isLast: {
    ...t.ml2
  }
})

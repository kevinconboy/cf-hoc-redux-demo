import Cookies from 'js-cookie'

const AUTH_COOKIE = 'cf_auth'

export function getAuthToken () {
  return Cookies.get(AUTH_COOKIE)
}

export function setAuthCookie (token) {
  // Max is one hour for the token, I set 15 minutes so its easier to test
  const minutes = 15
  const expires = new Date(new Date().getTime() + minutes * 60 * 1000)

  Cookies.set(AUTH_COOKIE, token, {
    expires: expires
  })

  return expires
}

export function redirectToLoginIfNeeded () {
  const authToken = getAuthToken()
  if (!authToken) {
    console.log('No Auth on withData component redirect to login')
    const location = window.location
    const redirect = encodeURIComponent(`${location.pathname}${location.search}${location.hash}`)

    location.href = `/login?redirect=/${redirect}`
  }
}

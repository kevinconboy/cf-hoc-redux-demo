import {ApolloClient} from 'react-apollo'
import {createNetworkInterface} from 'apollo-client'
import fetch from 'isomorphic-fetch'
import {getAuthToken} from '@/auth'

let apolloClient = null

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = fetch
}

function create () {
  const prodSuffix = process.env.NODE_ENV === 'production' ? '/api' : ''
  const suffix = `${prodSuffix}/graphql`
  const graphqlUrl = `https://${process.env.GRAPHQL_CLIENT}:${process.env.GRAPHQL_CLIENT_PORT}${suffix}`
  console.log('Create Apollo client:', graphqlUrl)
  const authToken = getAuthToken()

  const networkInterface = createNetworkInterface({
    uri: graphqlUrl, // Server URL (must be absolute)
    opts: { // Additional fetch() options like `credentials` or `headers`
      credentials: 'same-origin'
    }
  })

  networkInterface.use([{
    applyMiddleware (req, next) {
      if (!req.options.headers && authToken) {
        req.options.headers = {
          Authorization: authToken
        }
      }

      next()
    }
  }])

  return new ApolloClient({
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    networkInterface: networkInterface
  })
}

export default function initApollo () {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create()
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create()
  }

  return apolloClient
}

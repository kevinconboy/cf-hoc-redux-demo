import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import {navReducer, userReducer, colorsReducer, dialogReducer, authReducer} from './store'
import initialReduxState from './initialState'
import thunkMiddleware from 'redux-thunk'
import {setAuthCookie} from '@/auth'

let reduxStore = null

// Get the Redux DevTools extension and fallback to a no-op function
let devtools = f => f
if (process.browser && window.__REDUX_DEVTOOLS_EXTENSION__) {
  devtools = window.__REDUX_DEVTOOLS_EXTENSION__()
}

function create (apollo, initialState = initialReduxState) {
  return createStore(
    combineReducers({ // Setup reducers
      nav: navReducer,
      user: userReducer,
      colors: colorsReducer,
      dialog: dialogReducer,
      auth: authReducer,
      apollo: apollo.reducer()
    }),
    initialState, // Hydrate the store with server-side data
    compose(
      applyMiddleware(apollo.middleware(), thunkMiddleware), // Add additional middleware here
      devtools
    )
  )
}

export default function initRedux (apollo, initialState) {
  // Make sure to create a new store for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(apollo, initialState)
  }

  // Reuse store on the client-side
  if (!reduxStore) {
    reduxStore = create(apollo, initialState)
  }

  return reduxStore
}

export const toggleNav = direction => {
  const state = reduxStore.getState()
  const nav = Object.assign([], state.nav)
  if (direction === `COLLAPSE`) {
    nav.collapsed = true
  } else {
    nav.collapsed = false
  }

  return reduxStore.dispatch({type: `TOGGLE_NAV`, nav: nav})
}

export const toggleDialog = direction => {
  const state = reduxStore.getState()
  const dialog = Object.assign([], state.dialog)
  if (direction === `POP`) {
    dialog.popped = true
  } else {
    dialog.popped = false
  }

  return reduxStore.dispatch({type: `TOGGLE_DIALOG`, dialog: dialog})
}

export const setNavLevel = level => {
  const state = reduxStore.getState()
  const nav = Object.assign([], state.nav)
  nav.level = level
  return reduxStore.dispatch({type: `SET_NAV_LEVEL`, nav: nav})
}

export const handleLogin = (username, token) => {
  const expires = setAuthCookie(token)
  const auth = {
    token,
    expires,
    username
  }

  return reduxStore.dispatch({type: `HANDLE_LOGIN`, auth: auth})
}

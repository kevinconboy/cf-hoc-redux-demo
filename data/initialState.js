import Color from 'color'

export default {
  nav: {
    icon: `cf-network-logo.png`,
    title: `Community Funded Network`,
    collapsed: false,
    theme: `default`,
    level: 1
  },
  user: {
    name: `Sharen Lovejoy`,
    uuid: 1234567890,
    level: 1,
    avatar: `/static/img/CF-Admin-Proto-Map/UserAvatars/face 12.png`,
    title: `CF Success Manager`
  },
  dialog: {
    popped: false,
  },
  colors: [
    {
      primary: Color(`#18E63F`),
      secondary: Color(`#23A7AE`),
      button: Color(`#44A748`),
      alert: Color(`#FF0000`),
      plum: Color(`#766592`),
      grey: Color(`#797979`),
      blueGrey: Color(`#526184`),
      black: Color(`#222`),
      white: Color(`#FFF`)
    },
    {
      primary: Color(`#18E63F`),
      secondary: Color(`#23A7AE`),
      button: Color(`#44A748`),
      alert: Color(`#FF0000`),
      plum: Color(`#766592`),
      grey: Color(`#797979`),
      blueGrey: Color(`#526184`),
      black: Color(`#222`),
      white: Color(`#FFF`)
    },
    {
      primary: Color(`#9b37ff`),
      secondary: Color(`#ffe352`),
      button: Color(`#6D4793`),
      alert: Color(`#FF0000`),
      plum: Color(`#766592`),
      grey: Color(`#797979`),
      blueGrey: Color(`#526184`),
      black: Color(`#222`),
      white: Color(`#FFF`)
    },
    {
      primary: Color(`#0094ff`),
      secondary: Color(`#4efff4`),
      button: Color(`#399234`),
      alert: Color(`#FF0000`),
      plum: Color(`#766592`),
      grey: Color(`#797979`),
      blueGrey: Color(`#526184`),
      black: Color(`#222`),
      white: Color(`#FFF`)
    },
    {
      primary: Color(`#0094ff`),
      secondary: Color(`#4efff4`),
      button: Color(`#399234`),
      alert: Color(`#FF0000`),
      plum: Color(`#766592`),
      grey: Color(`#797979`),
      blueGrey: Color(`#526184`),
      black: Color(`#222`),
      white: Color(`#FFF`)
    }
  ],
  auth: {
    token: null,
    expires: null,
    username: null
  }
}

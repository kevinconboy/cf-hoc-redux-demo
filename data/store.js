import {combineReducers} from 'redux'
import initialState from './initialState'

export const navReducer = (state, action) => {
  if (!state) {
    state = initialState.nav
  }

  switch (action.type) {
    case `TOGGLE_NAV`: return action.nav
    case `SET_NAV_LEVEL`: return action.nav
    default: return state
  }
}

export const dialogReducer = (state, action) => {
  if (!state) {
    state = initialState.dialog
  }

  switch (action.type) {
    case `TOGGLE_DIALOG`: return action.dialog
    default: return state
  }
}

export const userReducer = (state, action) => {
  if (!state) {
    state = initialState.user
  }
  switch (action.type) {
    case 'HANDLE_LOGIN' : {
      console.log('userReducer:HANDLE_LOGIN', action)
      const auth = action.auth
      const username = auth.username
      return Object.assign({}, state, {
          name: username,
          uuid: 1234567890,
          level: 1,
          avatar: `/static/img/CF-Admin-Proto-Map/UserAvatars/face 12.png`,
          title: `USER LOGIN TODO`
      })
    }
    default: return state
  }
}

export const authReducer = (state, action) => {
  if (!state) {
    state = initialState.auth
  }
  switch (action.type) {
    case 'HANDLE_LOGIN' : {
      console.log('authReducer:HANDLE_LOGIN', action)
      return action.auth
    }
    default: return state
  }
}

export const colorsReducer = (state, action) => {
  if (!state) {
    state = initialState.colors
  }
  switch (action.type) {
    default: return state
  }
}

export const reducers = combineReducers({
  nav: navReducer, user: userReducer, colors: colorsReducer, dialog: dialogReducer, auth: authReducer
})

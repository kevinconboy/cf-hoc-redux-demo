module.exports = {
  'process.env.ENVIRONMENT': process.env.ENVIRONMENT || 'qa',
  'process.env.NODE_ENV': process.env.NODE_ENV || 'dev',
  'process.env.GRAPHQL_CLIENT': process.env.GRAPHQL_CLIENT || 'qa-api.communityfunded.com',
  'process.env.GRAPHQL_CLIENT_PORT': process.env.GRAPHQL_CLIENT_PORT || 443
}

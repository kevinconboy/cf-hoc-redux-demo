import Document, {Head, Main, NextScript} from 'next/document'
import {StyleSheetServer} from 'aphrodite/no-important'

export default class MyDocument extends Document {
  static async getInitialProps ({renderPage}) {
    const {html, css} = StyleSheetServer.renderStatic(() => renderPage())
    const ids = css.renderedClassNames
    return {...html, css, ids}
  }

  constructor (props) {
    super(props)
    const {__NEXT_DATA__, ids} = props
    if (ids) {
      __NEXT_DATA__.ids = this.props.ids
    }
  }

  render () {
    return (
      <html lang="en-US">
        <Head>
          <title>Community Funded // Manage</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="stylesheet" href="/static/tachyons.css" type="text/css" media="screen" />
          <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" />
          <style data-aphrodite dangerouslySetInnerHTML={{__html: this.props.css.content}} />
        </Head>
        <body style={{margin: 0, padding: 0}}>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

import Color from 'color'

export const baseColors = {
  primary: Color(`#23A7AE`),
  secondary: Color(`#18E63F`),
  button: Color(`#44A748`),
  confirm: Color(`#44A748`),
  alert: Color(`#FF0000`),
  warning: Color(`#F2C10E`),
  plum: Color(`#766592`),
  grey: Color(`#797979`),
  blueGrey: Color(`#526184`),
  black: Color(`#222`),
  white: Color(`#FFF`)
}

const valueFamilies = {
  1000: 0.9,
  975: 0.75,
  950: 0.5,
  900: 0.4,
  800: 0.3,
  700: 0.2,
  600: 0.1,
  500: 0,
  400: 0.1,
  300: 0.2,
  200: 0.3,
  100: 0.4,
  50: 0.5,
  25: 0.75,
  10: 0.9,
  5: 0.95
}

function calculateColors (colorSet) {
  if (!colorSet) {
    colorSet = baseColors
  } else {
    colorSet = Object.assign({}, colorSet)
  }
  const colors = {}

  colorSet.grey = colorSet.grey.mix(colorSet.primary, 0.15)

  for (const color in colorSet) {
    for (const level in valueFamilies) {
      if (parseInt(level) === 500) {
        colors[color + level] = colorSet[ color ].rgb().string()
        colors[`${color + level}_50`] = colorSet[ color ].fade(0.5).rgb().string()
        colors[`${color + level}_25`] = colorSet[ color ].fade(0.75).rgb().string()
        colors[`${color + level}_10`] = colorSet[ color ].fade(0.9).rgb().string()
      } else if (level > 500) {
        colors[color + level] = colorSet[ color ].darken(valueFamilies[ level ]).rgb().string()
      } else if (level < 500) {
        colors[color + level] = colorSet[ color ].lighten(valueFamilies[ level ]).rgb().string()
      }
    }
  }

  return colors
}

function getColorFamily (thisColor) {
  const localColors = {}
  for (const color in colors) {
    if (color.indexOf(thisColor) > -1) {
      localColors[ color.replace(thisColor, ``) ] = colors[ color ]
    }
  }
  return localColors
}

function createColorFamilies () {
  const localColors = []

  for (const color in baseColors) {
    localColors.push({
      name: color,
      family: getColorFamily(color)
    })
  }

  return localColors
}

const colors = calculateColors()
const styleguideColors = createColorFamilies(colors)

export {calculateColors, colors, styleguideColors}
